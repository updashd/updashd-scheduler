<?php
namespace Updashd\Scheduler\TestUtils;

class Strings {

    public static function generateString (int $length, array $charSet = [], $includeSpecial = false) {
        if (count($charSet) == 0) {
            $charSet = [];
            $charSet = array_merge($charSet, str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'));

            if ($includeSpecial) {
                $charSet = array_merge($charSet, str_split('`~!@#$%^&*()_+-=[]\{}|:";\'<>?,./'));
            }
        }

        $str = '';

        while(strlen($str) < $length) {
            $str .= $charSet[array_rand($charSet)];
        }

        return $str;
    }

}