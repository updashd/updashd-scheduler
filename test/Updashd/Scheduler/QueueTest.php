<?php
namespace Updashd\Scheduler;

use PHPUnit\Framework\TestCase;

class QueueTest extends TestCase {

    public function testGetProcessingKeyName () {
        $str = Queue::getProcessingKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetErrorKeyName () {
        $str = Queue::getErrorKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetZoneKeyName () {
        $str = Queue::getZoneKeyName('test', Queue::QUEUE_COMPLETE);
        self::assertNotEmpty($str);
    }

    public function testGetScheduleKeyName () {
        $str = Queue::getScheduleKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetServiceRegisterKeyName () {
        $str = Queue::getServiceRegisterKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetIncidentKeyName () {
        $str = Queue::getIncidentKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetRescheduleKeyName () {
        $str = Queue::getRescheduleKeyName('test');
        self::assertNotEmpty($str);
    }

    public function testGetPendingKeyName () {
        $str = Queue::getPendingKeyName('test', 'http');
        self::assertNotEmpty($str);
    }

    public function testGetZoneServiceKeyName () {
        $str = Queue::getZoneServiceKeyName('test', 'http', Queue::QUEUE_ERROR);
        self::assertNotEmpty($str);
    }

    public function testGetCompleteKeyName () {
        $str = Queue::getCompleteKeyName('test');
        self::assertNotEmpty($str);
    }
}
