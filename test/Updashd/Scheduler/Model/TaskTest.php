<?php
namespace Updashd\Scheduler\Model;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Updashd\Scheduler\Mock\MockClient;
use Updashd\Scheduler\ScheduleType;
use Updashd\Scheduler\State;

class TaskTest extends TestCase {

    /** @var \Updashd\Scheduler\Communication\ClientInterface */
    private $client;

    public function setUp () : void {
        $this->client = new MockClient();
    }

    public function testSettersGetters () {
        $task = new Task($this->client, 'test', 'a1');

        $task->setEnabled(true);
        self::assertTrue($task->getEnabled());

        $task->getState();
        $task->setState(State::STATE_UNDEFINED);
        self::assertEquals(State::STATE_UNDEFINED, $task->getState());

        $task->getScheduledTime();
        $task->setScheduledTime(10);
        self::assertEquals(10, $task->getScheduledTime());

        $task->getLastCompletionTime();
        $task->setLastCompletionTime(20);
        self::assertEquals(20, $task->getLastCompletionTime());

        $task->setTimeout(30);
        self::assertEquals(30, $task->getTimeout());

        $task->getScheduleType();
        $task->setScheduleType(ScheduleType::TYPE_ONCE);
        self::assertEquals(ScheduleType::TYPE_ONCE, $task->getScheduleType());

        $task->getCron();
        $task->setCron('* * * * *');
        self::assertEquals('* * * * *', $task->getCron());

        $task->getInterval();
        $task->setInterval(100);
        self::assertEquals(100, $task->getInterval());

        $task->getRunCount();
        $task->setRunCount(123);
        self::assertEquals(123, $task->getRunCount());

        $task->setConfig('{}');
        self::assertEquals('{}', $task->getConfig());

        $task->setService('test_service');
        self::assertEquals('test_service', $task->getService());

        $task->setAccountId('abc123');
        self::assertEquals('abc123', $task->getAccountId());
    }

    public function testDoubleSave () {
        $task = new Task($this->client, 'test', 'a1');
        self::assertTrue($task->isEntityDirty());

        $task->save();
        self::assertFalse($task->isEntityDirty());

        $task->save();
        self::assertFalse($task->isEntityDirty());
    }

    public function testRetrieveField () {
        $task = new Task($this->client, 'test', 'a1');
        $task->setConfig('this is a test value');
        $task->save();

        $nTask = new Task($this->client, 'test', 'a1');
        $nTask->retrieveField($nTask::FIELD_CONFIG);

        self::assertEquals($nTask->getConfig(), $task->getConfig());
    }

    public function testRetrieveExists () {
        $task = $this->createTask();

        // Save it
        $task->save();

        // Load it
        $nTask = new Task($this->client, 'test', 'a1');
        $nTask->retrieve();

        // Fresh loaded entity should not be dirty
        self::assertFalse($task->isEntityDirty());

        // They should match
        self::assertEquals($task, $nTask);
    }

    public function testRetrieveNonExistent () {
        $task = new Task($this->client, 'test', 'a1');
        $task->retrieve();

        self::assertTrue($task->isEntityDirty());
    }

    public function testIsDirty () {
        $task = new Task($this->client, 'test', 'a1');

        // Entity should be dirty when it's new
        self::assertTrue($task->isEntityDirty());

        // Save the entity
        $task->save();

        // Entity should be clean now
        self::assertFalse($task->isEntityDirty());

        $task->setConfig('test');

        // Entity should be dirty again
        self::assertTrue($task->isEntityDirty());

        // Individual fields should be right
        foreach ($task->getFields() as $field) {
            self::assertEquals(($field == $task::FIELD_CONFIG), $task->isFieldDirty($field));
        }

        // Save the config field
        $task->saveField($task::FIELD_CONFIG);

        // Entity should be clean now
        self::assertFalse($task->isEntityDirty());
    }

    public function testInvalidState () {
        $task = new Task($this->client, 'test', 'a1');
        $this->expectException(InvalidArgumentException::class);
        $task->setState('i_dont_exist');
    }

    public function testInvalidScheduleType () {
        $task = new Task($this->client, 'test', 'a1');
        $this->expectException(InvalidArgumentException::class);
        $task->setScheduleType('i_dont_exist');
    }

    public function testToArray () {
        $task = $this->createTask();

        self::assertNotNull($task->getArray());

        $arr = $task->toArray();
        self::assertNotNull($arr);
        self::assertEquals($task->getFields(), array_keys($arr));

        foreach ($arr as $field => $value) {
            self::assertEquals($task->getValue($field),  $value);
        }
    }

    public function testToString () {
        $task = $this->createTask();

        $str = $task->__toString();

        self::assertNotNull($str);
        self::assertGreaterThan(0, strlen($str));
    }

    public function testEmptyZone () {
        $this->expectException(InvalidArgumentException::class);
        new Task($this->client, '', 'test');
    }


    public function testSetEmptyZone () {
        $this->expectException(InvalidArgumentException::class);
        $task = new Task($this->client, 'test', 'test');
        $task->setZone('');
    }

    public function testEmptyId () {
        $this->expectException(InvalidArgumentException::class);
        new Task($this->client, 'test', '');
    }

    public function testSetEmptyId () {
        $this->expectException(InvalidArgumentException::class);
        $task = new Task($this->client, 'test', 'hi');
        $task->setId('');
    }

    /**
     * @return \Updashd\Scheduler\Model\Task
     */
    private function createTask () : Task {
        // Create a task
        $task = new Task($this->client, 'test', 'a1');
        $task->setEnabled(true);
        $task->setState(State::STATE_UNDEFINED);
        $task->setScheduledTime(10);
        $task->setLastCompletionTime(20);
        $task->setTimeout(30);
        $task->setScheduleType(ScheduleType::TYPE_ONCE);
        $task->setCron('* * * * *');
        $task->setInterval(100);
        $task->setRunCount(123);
        $task->setConfig('{}');
        $task->setService('test_service');
        $task->setAccountId('abc123');

        return $task;
    }
}
