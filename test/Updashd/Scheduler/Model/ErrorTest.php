<?php
namespace Updashd\Scheduler\Model;

use PHPUnit\Framework\TestCase;
use Updashd\Scheduler\Mock\MockClient;

class ErrorTest extends TestCase {

    const TEST_ZONE = 'test';

    /** @var \Updashd\Scheduler\Communication\ClientInterface */
    protected $client;

    public function setUp () : void {
        $this->client = new MockClient();
    }

    public function testCreate () {
        $taskId = 't1';
        $error = $this->createError($taskId);
        self::assertEquals(Error::NAME, $error->getName());
        self::assertEquals(self::TEST_ZONE, $error->getZone());
    }

    public function testFields () {
        $error = $this->createError('t1');

        $this->populateTest($error);
    }

    public function testSaveLoad () {
        $error = $this->createError('t2');

        $this->populateTest($error);

        // Save
        $error->save();

        // Retrieve
        $error2 = $this->createError('t2');
        $error2->retrieve();

        // Compare
        self::assertEquals($error, $error2);
    }

    public function testKeyName () {
        $key = Error::getErrorKeyName('test', 'abc');
        self::assertNotNull($key);
        self::assertGreaterThan(0, strlen($key), 'length must be > 0');
    }

    /**
     * @param \Updashd\Scheduler\Model\Error $error
     */
    protected function populateTest (Error $error) : void {
        $taskId = 'abc123';
        $error->setTaskId($taskId);
        self::assertEquals($taskId, $error->getTaskId());

        $now = time();
        $error->setDate($now);
        self::assertEquals($now, $error->getDate());

        $code = 100;
        $error->setErrorCode($code);
        self::assertEquals($code, $error->getErrorCode());

        $details = 'Testing 1 2 3.';
        $error->setErrorDetails($details);
        self::assertEquals($details, $error->getErrorDetails());
    }

    protected function createError ($id) : Error {
        return new Error($this->client, self::TEST_ZONE, $id);
    }
}
