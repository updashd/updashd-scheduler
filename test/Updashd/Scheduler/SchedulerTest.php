<?php
namespace Updashd\Scheduler;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Updashd\Scheduler\Mock\MockClient;
use Updashd\Scheduler\Popo\Service;
use Updashd\Scheduler\Scheduler\SchedulerInterface;
use Updashd\Scheduler\Scheduler\SchedulerImpl;
use Updashd\Scheduler\Communication\PredisClientProxy;

class SchedulerTest extends TestCase {
    const ZONE = 'test';

    public function schedulerProvider () {
        $client = new \Predis\Client('tcp://127.0.0.1:6379');
        return [
            [new SchedulerImpl(new MockClient(), self::ZONE)],
            [new SchedulerImpl(new PredisClientProxy($client), self::ZONE)]
        ];
    }

    /**
     * @dataProvider schedulerProvider
     */
    public function testCreateTaskFail (SchedulerInterface $scheduler) {
        $this->expectException(InvalidArgumentException::class);
        $scheduler->createTask('');
    }

    /**
     * @dataProvider schedulerProvider
     */
    public function testCreateResultFail (SchedulerInterface $scheduler) {
        $this->expectException(InvalidArgumentException::class);
        $scheduler->createResult('');
    }


    /**
     * @dataProvider schedulerProvider
     */
    public function testCreateErrorFail (SchedulerInterface $scheduler) {
        $this->expectException(InvalidArgumentException::class);
        $scheduler->createError('');
    }


    /**
     * @dataProvider schedulerProvider
     */
    public function testRegisterFlow (SchedulerInterface $scheduler) {
        $service = new Service('test', 'Test');
        $service->addFieldStr('check1', 'Check #1');

        $scheduler->registerService($service);

        // Try the blocking version
        $nService = $scheduler->getServiceRegistrationBlocking();
        self::assertNotNull($nService);
        self::assertEquals($service, $nService);

        // Re-register and try the timeout version
        $scheduler->registerService($service);

        // Get it
        $nService = $scheduler->getServiceRegistrationOrTimeout(1);
        self::assertNotNull($nService);
        self::assertEquals($service, $nService);

        // Get nothing
        $nService = $scheduler->getServiceRegistrationOrTimeout(1);
        self::assertNull($nService);

        // Get nothing
        $nService = $scheduler->getServiceRegistrationQuick(1);
        self::assertFalse($nService);
    }


    /**
     * @dataProvider schedulerProvider
     */
    public function testTaskCreateGet (SchedulerInterface $scheduler) {
        $taskId = 'task1';
        $service = 'test_service';
        $interval = 10;

        $task = $scheduler->createTask($taskId);
        $this->populateTask($scheduler, $task, $interval, $service);
        $scheduler->addTask($task);

        $nTask = $scheduler->getTaskEntity($taskId);

        self::assertNotNull($nTask);
        self::assertEquals($task->getScheduleType(), $nTask->getScheduleType());
        self::assertEquals($task->getAccountId(), $nTask->getAccountId());
        self::assertEquals($task->getConfig(), $nTask->getConfig());
        self::assertEquals($task->getInterval(), $nTask->getInterval());
    }


    /**
     * @dataProvider schedulerProvider
     * @throws \Exception
     */
    public function testTaskFlow (SchedulerInterface $scheduler) {
        $taskId = 'task1';
        $service = 'test_service';
        $interval = 10;

        $task = $scheduler->createTask($taskId);
        $this->populateTask($scheduler, $task, $interval, $service);
        $scheduler->addTask($task);


        $nTask = $scheduler->getTaskEntity($taskId);
        $this->assertTaskState($scheduler, $taskId, State::STATE_RESCHEDULING);
        self::assertEquals(0, $nTask->getRunCount());
        self::assertNull($nTask->getLastCompletionTime());

        // Schedule the task
        $scheduler->processReschedule(false, 1);

        $nTask = $scheduler->getTaskEntity($taskId);
        $this->assertTaskState($scheduler, $taskId, State::STATE_SCHEDULED);
        self::assertNotNull($nTask->getScheduledTime());
        self::assertGreaterThan(time(), $nTask->getScheduledTime());

        // Queue task for execution
        $scheduler->processSchedule(time() + $interval);
        $this->assertTaskState($scheduler, $taskId, State::STATE_PENDING);

        $nTask = $scheduler->getTaskEntity($taskId);
        self::assertNotNull($nTask->getScheduledTime());
        self::assertGreaterThan(time(), $nTask->getScheduledTime());

        // Get task as if we are a worker
        $task = $scheduler->getTask([$service]);
        self::assertNotNull($task);
        
        // Mark as being processed
        $result = $scheduler->markProcessing($task);
        $this->assertTaskState($scheduler, $taskId, State::STATE_PROCESSING);

        // Here is where thinks would be done

        // Store and report the result
        $result->setResult(json_encode(['status' => 'done']));

        $scheduler->recordResult($result);
        $this->assertTaskState($scheduler, $taskId, State::STATE_COMPLETE);
        self::assertLessThanOrEqual(1, time() - $scheduler->getTaskEntity($taskId)->getLastCompletionTime());
        self::assertEquals(1, $scheduler->getTaskEntity($taskId)->getRunCount());

        // Mark the task as completed
        $scheduler->markComplete($task);
        $this->assertTaskState($scheduler, $taskId, State::STATE_RESCHEDULING);

        $scheduler->processReschedule(false, 1);
        $scheduler->processSchedule(time() + $interval);

        // Try to get task
        $task = $scheduler->getTaskNonBlocking([$service]);
        self::assertNotNull($task);

        // Try to get task
        $task = $scheduler->getTaskNonBlocking([$service]);
        self::assertNull($task);
    }


    /**
     * @param string $taskId
     * @param string $state
     */
    private function assertTaskState ($scheduler, string $taskId, string $state) : void {
        $nTask = $scheduler->getTaskEntity($taskId);
        self::assertEquals($state, $nTask->getState());
    }


    /**
     * @param \Updashd\Scheduler\Model\Task $task
     * @param int $interval
     * @param string $service
     */
    private function populateTask ($scheduler, Model\Task $task, int $interval, string $service) : void {
        $task->setEnabled(true);
        $task->setScheduleType(ScheduleType::TYPE_SECOND_INTERVAL);
        $task->setInterval($interval);
        $task->setService($service);
        $task->setAccountId(1);
        $task->setConfig('config');
    }
}
