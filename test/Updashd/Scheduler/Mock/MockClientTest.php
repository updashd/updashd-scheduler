<?php

namespace Updashd\Scheduler\Mock;

use PHPUnit\Framework\TestCase;

class MockClientTest extends TestCase {

    /** @var \Updashd\Scheduler\Mock\MockClient */
    private $client;

    public function setUp () : void {
        $this->client = new MockClient();
    }

    public function testInvalidKey () {
        $this->expectException(\Exception::class);
        $this->client->llen('');
    }

    public function testExpire () {
        $c = $this->client;

        $key = 'test';
        $value = 'Hi';
        $field = 'a';

        // Expire non-existent thing
        $c->expire($key, 0);

        self::assertNull($c->hget($key, $field));

        $c->hset($key, $field, $value);

        self::assertNotNull($c->hget($key, $field));

        $seconds = 2;

        $c->expire($key, $seconds);

        self::assertEquals($seconds, $c->ttl($key));

        self::assertNotNull($c->hget($key, $field));

        $c->expire($key, 0);

        self::assertNull($c->hget($key, $field));
    }

    public function testPersist () {
        $c = $this->client;

        $c->persist('test');

        $key = 'test';
        $value = 'Hi';
        $field = 'a';

        $c->hset($key, $field, $value);

        $c->expire($key, 2);

        self::assertEquals(2, $c->ttl($key));

        $c->persist($key);

        self::assertEquals(-1, $c->ttl($key));
    }

    public function testTtl () {
        $c = $this->client;

        $key = 'test';

        self::assertEquals(-2, $c->ttl($key));

        $c->hset($key, 'a', 'Hi');

        self::assertEquals(-1, $c->ttl($key));

        $c->expire($key, 2);

        self::assertEquals(2, $c->ttl($key));
    }

    public function testBrpop () {
        $c = $this->client;

        self::assertNull($c->brpop(['test'], 1));

        $c->lpush('test', '1');
        $c->lpush('test', '2');

        $out = $c->brpop('test', 1);
        self::assertNotNull($out);

        list($key, $value) = $out;
        self::assertEquals('test', $key);
        self::assertEquals('1', $value);

        $c->brpop(['test'], 1);
        $out = $c->brpop(['test'], 1);
        self::assertNull($out);
    }

    public function testLpush () {
        $c = $this->client;

        $key = 'test';

        self::assertEmpty($c->ldebug($key));
        self::assertEquals(0, $c->llen($key));

        $c->lpush($key, 1);
        self::assertEquals(1, $c->llen($key));
        self::assertEquals([1], $c->ldebug($key));

        $c->lpush($key, [2,3]);
        self::assertEquals(3, $c->llen($key));
        self::assertNotEquals([1,2,3], $c->ldebug($key));
        self::assertEquals([3,2,1], $c->ldebug($key));
    }

    public function testLremA () {
        $c = $this->client;
        $key = 'test';
        $data = [1, 2, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9, 9];
        // Insert sample data
        $c->lpush($key, array_reverse($data));
        self::assertEquals($data, $c->ldebug($key));

        // Remove all 6s
        $exp = [1, 2, 1, 2, 3, 4, 3, 4, 5, 7, 8, 9, 9];
        $cnt = $c->lrem($key, 0, 6);
        self::assertEquals(2, $cnt);
        self::assertEquals(count($exp), $c->llen($key));
        self::assertEquals($exp, $c->ldebug($key));
    }

    public function testLremB () {
        $c = $this->client;
        $key = 'test';
        $data = [1, 2, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9, 9];
        // Insert sample data
        $c->lpush($key, array_reverse($data));
        self::assertEquals($data, $c->ldebug($key));

        // Remove one 1 from left-hand side
        $exp = [2, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9, 9];
        $cnt = $c->lrem($key, 1, 1);
        self::assertEquals(1, $cnt);
        self::assertEquals(count($exp), $c->llen($key));
        self::assertEquals($exp, $c->ldebug($key));

        // Remove one 1 from left-hand side again
        $exp = [2, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9, 9];
        $cnt = $c->lrem($key, 1, 1);
        self::assertEquals(1, $cnt);
        self::assertEquals(count($exp), $c->llen($key));
        self::assertEquals($exp, $c->ldebug($key));
    }

    public function testLremC () {
        $c = $this->client;
        $key = 'test';
        $data = [1, 2, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9, 9];
        // Insert sample data
        $c->lpush($key, array_reverse($data));
        self::assertEquals($data, $c->ldebug($key));

        // Remove one 9 from right-hand side
        $exp = [1, 2, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 9];
        $cnt = $c->lrem($key, -1, 9);
        self::assertEquals(1, $cnt);
        self::assertEquals(count($exp), $c->llen($key));
        self::assertEquals($exp, $c->ldebug($key));

        // Remove one 4 from right-hand side
        $exp = [1, 2, 1, 2, 3, 4, 3, 5, 6, 6, 7, 8, 9];
        $cnt = $c->lrem($key, -1, 4);
        self::assertEquals(1, $cnt);
        self::assertEquals(count($exp), $c->llen($key));
        self::assertEquals($exp, $c->ldebug($key));
    }

    public function testLremD () {
        $out = $this->client->lrem('test', 1, 1);
        self::assertEquals(0, $out);
    }

    public function testHgetSetLen () {
        $c = $this->client;

        $key = 'test';
        $field = 'a';
        $fieldB = 'b';

        // Doesn't exist yet
        self::assertNull($c->hget($key, $field));
        self::assertEquals(0, $c->hlen($key));

        // Creation should return 1
        self::assertEquals(1, $c->hset($key, $field, 'value'));
        self::assertEquals(1, $c->hlen($key));
        self::assertEquals('value', $c->hget($key, $field));

        // Update should return 0
        self::assertEquals(0, $c->hset($key, $field, 'value2'));
        self::assertEquals(1, $c->hlen($key));
        self::assertEquals('value2', $c->hget($key, $field));

        // Update should return 0
        self::assertEquals(1, $c->hset($key, $fieldB, 'valueB'));
        self::assertEquals(2, $c->hlen($key));
        self::assertEquals('valueB', $c->hget($key, $fieldB));
    }

    public function testHincrby () {
        $c = $this->client;

        $key = 'test';
        $fieldA = 'a';
        $fieldB = 'b';

        // Doesn't exist yet
        self::assertNull($c->hget($key, $fieldA));
        self::assertEquals(0, $c->hlen($key));

        // Set one
        self::assertEquals(1, $c->hset($key, $fieldA, 10));
        self::assertEquals(11, $c->hincrby($key, $fieldA, 1));
        self::assertEquals(11, $c->hget($key, $fieldA));
        self::assertEquals(13, $c->hincrby($key, $fieldA, 2));
        self::assertEquals(13, $c->hget($key, $fieldA));

        // Incrementing non-existent should default to 0 and then increment
        self::assertNull($c->hget($key, $fieldB));
        self::assertEquals(1, $c->hincrby($key, $fieldB, 1));
    }

    public function testHmGetSet () {
        $c = $this->client;
        $key = 'test';
        self::assertEquals([null, null], $c->hmget($key, ['1', '2']));

        $c->hmset($key, ['1' => '1', '2' => '2']);
        self::assertEquals(['1', '2'], $c->hmget($key, ['1', '2']));
        self::assertEquals(2, $c->hlen($key));
    }

    public function testZMockNotSupportedNonNumeric () {
        $this->expectException(\RuntimeException::class);
        $this->client->zadd('test2', ['A' => 'test']);
    }

    public function testZMockNotSupportedNonIntegerAdd () {
        $this->expectException(\RuntimeException::class);
        $this->client->zadd('test2', ['A' => 10.5]);
    }

    public function testZMockNotSupportedNonNumericRange () {
        $this->expectException(\RuntimeException::class);
        $this->client->zrevrangebyscore('test2', 'a', 'b');
    }

    public function testZMockNotSupportedNonIntegerRange () {
        $this->expectException(\RuntimeException::class);
        $this->client->zrevrangebyscore('test2', 10.5, 10.4);
    }

    public function testZMockNotSupportedOptions () {
        $this->expectException(\RuntimeException::class);
        $this->client->zrevrangebyscore('test2', 10.5, 10.4, []);
    }

    public function testZadd () {
        $c = $this->client;
        $key = 'test';

        self::assertEquals(0, $c->zcard($key));

        self::assertEquals(1, $c->zadd($key, ['A' => 10]));
        self::assertEquals(1, $c->zcard($key));
        self::assertEquals([10 => ['A']], $c->zdebug($key));

        self::assertEquals(1, $c->zadd($key, ['B' => 10]));
        self::assertEquals(2, $c->zcard($key));
        self::assertEquals([10 => ['A', 'B']], $c->zdebug($key));

        self::assertEquals(1, $c->zadd($key, ['C' => 11]));
        self::assertEquals(3, $c->zcard($key));
        self::assertEquals([10 => ['A','B'], 11 => ['C']], $c->zdebug($key));

        self::assertEquals(1, $c->zadd($key, ['D' => 13]));
        self::assertEquals(4, $c->zcard($key));
        self::assertEquals([10 => ['A','B'], 11 => ['C'], 13 => ['D']], $c->zdebug($key));

        // Insert something that already exists
        self::assertEquals(0, $c->zadd($key, ['A' => 13]));
        self::assertEquals(4, $c->zcard($key));
        self::assertEquals([10 => ['B'], 11 => ['C'], 13 => ['A', 'D']], $c->zdebug($key));
    }

    public function testZrem () {
        $c = $this->client;
        $key = 'test';

        self::assertNull($c->zdebug($key));

        self::assertEquals(4, $c->zadd($key, [
            'A' => 10,
            'B' => 11,
            'C' => 100,
            'D' => 10
        ]));

        self::assertEquals(4, $c->zcard($key));
        self::assertEquals([10 => ['A', 'D'], 11 => ['B'], 100 => ['C']], $c->zdebug($key));

        self::assertEquals(1, $c->zrem($key, 'A'));
        self::assertEquals(3, $c->zcard($key));
        self::assertEquals([10 => ['D'], 11 => ['B'], 100 => ['C']], $c->zdebug($key));

        self::assertEquals(1, $c->zrem($key, 'D'));
        self::assertEquals(2, $c->zcard($key));
        self::assertEquals([11 => ['B'], 100 => ['C']], $c->zdebug($key));

        self::assertEquals(1, $c->zrem($key, 'B'));
        self::assertEquals(1, $c->zcard($key));
        self::assertEquals([100 => ['C']], $c->zdebug($key));

        self::assertEquals(1, $c->zrem($key, 'C'));
        self::assertEquals(0, $c->zcard($key));
        self::assertEquals([], $c->zdebug($key));
    }

    public function testZrevrangebyscore () {
        $c = $this->client;
        $key = 'test';

        self::assertEquals(4, $c->zadd($key, [
            'A' => 10,
            'B' => 11,
            'C' => 100,
            'D' => 10
        ]));

        // Empty result set
        $values = $c->zrevrangebyscore($key, 300, 200);
        self::assertEmpty($values);

        // Check for highest
        $values = $c->zrevrangebyscore($key, 100, 50);
        self::assertEquals(['C'], $values);

        // Should still be there
        $values = $c->zrevrangebyscore($key, 100, 11);
        self::assertEquals(['C', 'B'], $values);

        // Should still be there
        $values = $c->zrevrangebyscore($key, 100, 0);
        self::assertEquals(['C', 'B', 'A', 'D'], $values);
    }

    public function testZcard () {
        $c = $this->client;
        $key = 'test';

        self::assertEquals(0, $c->zcard($key));

        self::assertEquals(1, $c->zadd($key, ['A' => 10]));
        self::assertEquals(1, $c->zcard($key));

        self::assertEquals(['10' => ['A']], $c->zdebug($key));
    }

    public function testMulti () {
        $c = $this->client;

        $c->multi();
        self::assertTrue($c->debugIsMulti());
        $c->exec();
        self::assertFalse($c->debugIsMulti());

        $c->multi();
        self::assertTrue($c->debugIsMulti());
        $c->discard();
        self::assertFalse($c->debugIsMulti());

        // Calling again doesn't change anything
        $c->multi();
        $c->multi();
        self::assertTrue($c->debugIsMulti());
        $c->discard();
        self::assertFalse($c->debugIsMulti());
    }

    public function testExec () {
        $this->expectException(\Exception::class);
        $this->client->exec();
    }

    public function testDiscard () {
        $this->expectException(\Exception::class);
        $this->client->discard();
    }

    public function testPublish () {
        self::assertEquals(-1, $this->client->publish('test', 'test'));
    }
}
