<?php
namespace Updashd\Scheduler\Popo;

use PHPUnit\Framework\TestCase;
use Updashd\Scheduler\TestUtils\Strings;

class ServiceFieldTest extends TestCase {

    /** @var \Updashd\Scheduler\Popo\ServiceField */
    private $service;

    public function setUp () : void {
        $this->service = new ServiceField('test', 'Test', ServiceField::TYPE_STR, '');
    }

    /////////
    // setKey
    /////////
    public function testSetKeyFailEmpty() {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setKey('');
    }

    public function testSetKeyFailLong() {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setKey(Strings::generateString(ServiceField::KEY_MAX_LENGTH + 1));
    }

    public function testSetKeySuccess () {
        $this->service->setKey('test2');
        self::assertEquals('test2', $this->service->getKey());
    }

    //////////
    // setName
    //////////
    public function testSetNameFailEmpty () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setName('');
    }

    public function testSetNameFailLong () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setName(Strings::generateString(ServiceField::NAME_MAX_LENGTH + 1));
    }

    public function testSetNameSuccess () {
        $this->service->setName('Testing Again');
        self::assertEquals('Testing Again', $this->service->getName());
    }

    //////////
    // setType
    //////////
    public function testSetTypeFailEmpty () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setType('nothing');
    }

    public function testSetTypeFailLong () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setType(Strings::generateString(ServiceField::TYPE_MAX_LENGTH));
    }

    public function testSetTypeSuccess () {
        $this->service->setType(ServiceField::TYPE_TXT);
        self::assertEquals(ServiceField::TYPE_TXT, $this->service->getType());
    }

    //////////
    // setUnit
    //////////
    public function testSetUnitFailLong () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setUnit(Strings::generateString(ServiceField::UNIT_MAX_LENGTH + 1));
    }

    public function testSetUnitSuccess () {
        $unit = 'ms';

        $this->service->setUnit($unit);
        self::assertEquals($unit, $this->service->getUnit());
    }

    /////////////
    // fromArray
    /////////////
    public function testFromArrayFailIncompleteA () {
        $this->expectException(\InvalidArgumentException::class);
        ServiceField::fromArray([]);
    }

    public function testFromArrayFailIncompleteB () {
        $this->expectException(\InvalidArgumentException::class);
        ServiceField::fromArray([
            ServiceField::FIELD_KEY => 'key',
        ]);
    }

    public function testFromArrayFailIncompleteC () {
        $this->expectException(\InvalidArgumentException::class);
        ServiceField::fromArray([
            ServiceField::FIELD_NAME => 'name',
        ]);
    }

    public function testFromArraySuccess () {
        $field = ServiceField::fromArray([
            ServiceField::FIELD_KEY => 'key',
            ServiceField::FIELD_NAME => 'name',
            ServiceField::FIELD_TYPE => ServiceField::TYPE_INT,
            ServiceField::FIELD_UNIT => ''
        ]);

        self::assertNotNull($field);
    }

    //////////
    // toArray
    //////////
    public function testToArraySuccess () {
        $serviceField = new ServiceField('key', 'name', ServiceField::TYPE_INT, '');

        self::assertEquals([
            ServiceField::FIELD_KEY => 'key',
            ServiceField::FIELD_NAME => 'name',
            ServiceField::FIELD_TYPE => ServiceField::TYPE_INT,
            ServiceField::FIELD_UNIT => ''
        ], $serviceField->toArray());
    }
}
