<?php
namespace Updashd\Scheduler\Popo;
use PHPUnit\Framework\TestCase;
use Updashd\Scheduler\TestUtils\Strings;

class ServiceTest extends TestCase {

    /** @var \Updashd\Scheduler\Popo\Service */
    private $service;

    public function setUp () : void {
        $this->service = new Service('test', 'Test');
    }

    public function testSetModuleEmpty () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setModuleName('');
    }

    public function testSetModuleLong () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setModuleName(Strings::generateString(Service::MODULE_NAME_MAX_LENGTH + 1));
    }

    public function testSetModuleSuccess () {
        $this->service->setModuleName('testing');
        self::assertEquals('testing', $this->service->getModuleName());
    }

    public function testSetReadableNameEmpty () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setReadableName('');
    }

    public function testSetReadableNameLong () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setReadableName(Strings::generateString(Service::READABLE_NAME_MAX_LENGTH + 1));
    }

    public function testAddFieldFailNull () {
        $this->expectException(\TypeError::class);
        $this->service->addField(null);
    }

    public function testAddFieldFailString () {
        $this->expectException(\TypeError::class);
        $this->service->addField('I am a string');
    }

    public function testAddFieldSuccess () {
        self::assertNoFields();

        $name = 'Test 1';
        $key = 'test1';
        $type = ServiceField::TYPE_TXT;
        $unit = 'text';

        $field = new ServiceField($key, $name, $type, $unit);
        $this->service->addField($field);

        self::assertCount(1, $this->service->getFields());
        self::assertContains($field, $this->service->getFields());

        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testAddFieldQuickSuccess () {
        self::assertNoFields();

        $key = 'test2';
        $name = 'Test 2';
        $type = ServiceField::TYPE_FLOAT;
        $unit = '';

        $result = $this->service->addFieldQuick($key, $name, $type);

        self::assertFieldAdded($result);
        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testAddFieldIntSuccess () {
        self::assertNoFields();

        $key = 'test3';
        $name = 'Test 3';
        $type = ServiceField::TYPE_INT;
        $unit = 'ms';

        $result = $this->service->addFieldInt($key, $name, $unit);

        self::assertFieldAdded($result);
        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testAddFieldFloatSuccess () {
        self::assertNoFields();

        $key = 'test4';
        $name = 'Test 4';
        $type = ServiceField::TYPE_FLOAT;
        $unit = 'sec';

        $result = $this->service->addFieldFloat($key, $name, $unit);

        self::assertFieldAdded($result);
        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testAddFieldStrSuccess () {
        self::assertNoFields();

        $key = 'test5';
        $name = 'Test 5';
        $type = ServiceField::TYPE_STR;
        $unit = 'str';

        $result = $this->service->addFieldStr($key, $name, $unit);

        self::assertFieldAdded($result);
        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testAddFieldTxtSuccess () {
        self::assertNoFields();

        $key = 'test6';
        $name = 'Test 6';
        $type = ServiceField::TYPE_TXT;
        $unit = 'content';

        $result = $this->service->addFieldTxt($key, $name, $unit);

        self::assertFieldAdded($result);
        self::assertFirstField($key, $name, $type, $unit);
    }

    public function testGetFieldsAsArray () {
        $fieldsArray = $this->service->getFieldsAsArray();
        self::assertCount(0, $fieldsArray);

        $this->service->addFieldInt('int', 'Integer');

        $fieldsArray = $this->service->getFieldsAsArray();
        self::assertCount(1, $fieldsArray);
    }

    public function testSetFieldsFailEmpty () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setFields(['hi']);
    }

    public function testSetFieldsSuccess () {
        $this->service->setFields([new ServiceField('test', 'Test')]);

        self::assertCount(1, $this->service->getFields());
    }

    public function testSetFieldsFromArrayFailInvalidA () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setFieldsFromArray([[]]);
    }

    public function testSetFieldsFromArrayFailInvalidB () {
        $this->expectException(\InvalidArgumentException::class);
        $this->service->setFieldsFromArray([['something' => 'nothing']]);
    }

    public function testSetFieldsFromArraySuccessEmpty () {
        $this->service->setFieldsFromArray([]);
        self::assertCount(0, $this->service->getFields());
    }

    public function testSetFieldsFromArraySuccessOne () {
        $field = new ServiceField('abc', 'ABC');
        $this->service->setFieldsFromArray([$field->toArray()]);
        self::assertCount(1, $this->service->getFields());
        self::assertContainsEquals($field, $this->service->getFields());
    }

    public function testSetFieldsFromArraySuccessSeveral () {
        $field1 = new ServiceField('abc', 'ABC');
        $field2 = new ServiceField('def', 'DEF');
        $field3 = new ServiceField('ghi', 'ghi');
        $this->service->setFieldsFromArray([
            $field1->toArray(),
            $field2->toArray(),
            $field3->toArray(),
        ]);
        self::assertCount(3, $this->service->getFields());
        self::assertContainsEquals($field1, $this->service->getFields());
        self::assertContainsEquals($field2, $this->service->getFields());
        self::assertContainsEquals($field3, $this->service->getFields());
    }

    public function testToArray () {
        $field = $this->service->addFieldInt('int', 'Integer');

        $serviceArray = $this->service->toArray();

        self::assertArrayHasKey(Service::FIELD_MODULE_NAME, $serviceArray);
        self::assertArrayHasKey(Service::FIELD_READABLE_NAME, $serviceArray);
        self::assertArrayHasKey(Service::FIELD_FIELDS, $serviceArray);

        self::assertCount(1, $serviceArray[Service::FIELD_FIELDS]);
        self::assertEquals($field->toArray(), $serviceArray[Service::FIELD_FIELDS][0]);
    }

    public function testFromArrayFailA () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromArray([]);
    }

    public function testFromArrayFailB () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromArray([Service::FIELD_MODULE_NAME => 'a']);
    }

    public function testFromArrayFailC () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromArray([Service::FIELD_READABLE_NAME => 'b']);
    }

    public function testFromArrayFailD () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromArray([
            Service::FIELD_MODULE_NAME => 'name',
            Service::FIELD_READABLE_NAME => 'Name',
            Service::FIELD_FIELDS => 'str'
        ]);
    }

    public function testFromArraySuccessA () {
        $name = 'testA';
        $readableName = 'Test A';

        $service = Service::fromArray([
            Service::FIELD_MODULE_NAME => $name,
            Service::FIELD_READABLE_NAME => $readableName,
        ]);

        self::assertNotNull($service);
        self::assertEquals($name, $service->getModuleName());
        self::assertEquals($readableName, $service->getReadableName());
    }

    public function testFromArraySuccessB () {
        $this->service->setModuleName('testing');
        $this->service->setReadableName('Testing');
        $this->service->addFieldInt('num_tests', 'Number of Tests');
        $this->service->addFieldFloat('avg_result', 'Average Result');

        $input = $this->service->toArray();

        $nService = Service::fromArray($input);

        self::assertNotNull($nService);
        self::assertEquals($this->service, $nService);
    }

    public function testToJson () {
        $json = $this->service->toJson();

        self::assertNotNull($json);
        self::assertTrue(strlen($json) > 0, 'Length of json string must be greater than 0');
    }

    public function testFromJsonSuccess () {
        $json = $this->service->toJson();
        $service = Service::fromJson($json);

        self::assertNotNull($service);
        self::assertEquals($this->service->toArray(), $service->toArray());
    }

    public function testFromJsonFailNull () {
        $this->expectException(\TypeError::class);
        Service::fromJson(null);
    }

    public function testFromJsonFailEmptyString () {
        $this->expectException(\TypeError::class);
        Service::fromJson('');
    }

    public function testFromJsonFailEmptyObject () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromJson('{}');
    }

    public function testFromJsonFailEmptyArray () {
        $this->expectException(\InvalidArgumentException::class);
        Service::fromJson('[]');
    }


    ///////////////////////////////////////////////////////////////////////
    /// Helper Assertions
    ///////////////////////////////////////////////////////////////////////
    /**
     * @param \Updashd\Scheduler\Popo\ServiceField $result
     */
    protected function assertFieldAdded (ServiceField $result) : void {
        self::assertNotNull($result);
        self::assertCount(1, $this->service->getFields());
        self::assertContains($result, $this->service->getFields());
    }

    /**
     * @param string $key
     * @param string $name
     * @param string $type
     * @param string $unit
     */
    protected function assertFirstField (string $key, string $name, string $type, string $unit) : void {
        $serviceField = $this->service->getFields()[0];

        self::assertEquals($key, $serviceField->getKey());
        self::assertEquals($name, $serviceField->getName());
        self::assertEquals($type, $serviceField->getType());
        self::assertEquals($unit, $serviceField->getUnit());
    }

    protected function assertNoFields () : void {
        self::assertCount(0, $this->service->getFields());
    }
}