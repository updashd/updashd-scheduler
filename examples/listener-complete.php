<?php
require 'bootstrap.php';

if ($argc < 3) {
    echo 'Usage: ' . $argv[0] . ' <zone> <taskIds>' . PHP_EOL;
    echo 'Services is a comma separated variable of the list of taskIds to subscribe to changes for.' . PHP_EOL;
    die(1);
}

$zone = $argv[1];
$taskIds = $argv[2];

$taskIds = explode(',', $taskIds);

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');
$client2 = new \Predis\Client('tcp://127.0.0.1:6379?alias=two');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);
$scheduler2 = new \Updashd\Scheduler\Scheduler($client2, $zone);

$results = $scheduler->subscribeResult($taskIds);

foreach ($results as $result) {
    if ($result->kind == 'message') {
        $resultEntity = $scheduler2->getResultEntity($result->payload);
        print_r($resultEntity->getArray());
    }
}