<?php
require 'bootstrap.php';

if ($argc < 2) {
    echo 'Usage: ' . $argv[0] . ' <zone>' . PHP_EOL;
    die(1);
}

$zone = $argv[1];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

while ($result = $scheduler->getResultOrError()) {
    if ($result instanceof \Updashd\Scheduler\Model\Error) {
        printf("   Error: % 4s: %d - %s\n", $result->getId(), $result->getErrorCode(), $result->getErrorDetails());
        
        $scheduler->markErrorReported($result);
    }
    elseif ($result instanceof \Updashd\Scheduler\Model\Result) {
        printf("Complete: %s: %s to %s\n", $result->getId(), $result->getStartTime(), $result->getEndTime());
    
        $scheduler->markResultRecorded($result);
        $scheduler->publishResult($result);
    }
    else {
        // Unknown type
        printf("Unknown!");
    }
}