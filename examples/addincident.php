<?php
require 'bootstrap.php';

if ($argc < 2) {
    echo 'Usage: ' . $argv[0] . ' <zone>';
    die(1);
}

$zone = $argv[1];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');
$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

$incidentId = rand(1, 9999);
$scheduler->pushIncident($incidentId);

echo 'Zone: ' . $zone . PHP_EOL;
echo 'Incident ID: ' . $incidentId . PHP_EOL;
