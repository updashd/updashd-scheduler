<?php
require 'bootstrap.php';

if ($argc < 2) {
    echo 'Usage: ' . $argv[0] . ' <zone>';
    die(1);
}

$zone = $argv[1];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');
$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

$service = new \Updashd\Scheduler\Popo\Service('test', 'Test Service');
$service->addFieldStr('str_f', 'Test Str Field', '');
$service->addFieldTxt('text_f', 'Test Txt Field', '');

// Test Json encoding/decode
$json = $service->toJson();
$newService = \Updashd\Scheduler\Popo\Service::fromJson($json);

print_r($newService->toArray());

// Test Redis stuff.
$scheduler->registerService($service);
$resultRegisterField = $scheduler->getServiceRegistrationBlocking();
print_r($resultRegisterField->toArray());
