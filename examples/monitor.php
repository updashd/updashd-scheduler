<?php
require 'bootstrap.php';

if ($argc < 3) {
    echo 'Usage: ' . $argv[0] . ' <zone> <services>' . PHP_EOL;
    echo 'Services is a comma separated variable of the list of services the worker can support' . PHP_EOL;
    die(1);
}

$zone = $argv[1];
$services = explode(',', $argv[2]);

$client = new \Predis\Client('tcp://127.0.0.1:6379?connection_timeout=5&read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);
    
while (true) {
    echo "\033[2J";

    echo 'RES: ' . $scheduler->getRescheduleSize() . PHP_EOL;
    echo 'SCH: ' . $scheduler->getScheduleSize() . PHP_EOL;
    foreach ($services as $service) {
        $service = trim($service);
        
        echo 'PND: ' . $scheduler->getPendingSize($service) . ' ' . $service . PHP_EOL;
    }
    echo 'PRC: ' . $scheduler->getProcessingSize() . PHP_EOL;
    echo 'CMP: ' . $scheduler->getCompleteSize() . PHP_EOL;
    echo 'ERR: ' . $scheduler->getErrorSize() . PHP_EOL;
    echo 'ICD: ' . $scheduler->getIncidentSize() . PHP_EOL;
    echo 'REG: ' . $scheduler->getRegisterFieldSize() . PHP_EOL;

    usleep(500000);
}