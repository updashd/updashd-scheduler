<?php
require 'bootstrap.php';

if ($argc < 2) {
    echo 'Usage: ' . $argv[0] . ' <zone>';
    die(1);
}

$zone = $argv[1];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

while (true) {
    $time = time();
    $count = $scheduler->processSchedule($time);
    echo date('Y-m-d h:i:s', $time) . ' ' . $count . PHP_EOL;
    
    sleep(1);
}
