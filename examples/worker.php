<?php
require 'bootstrap.php';

if ($argc < 3) {
    echo 'Usage: ' . $argv[0] . ' <zone> <services>' . PHP_EOL;
    echo 'Services is a comma separated variable of the list of services the worker can support' . PHP_EOL;
    die(1);
}

$zone = $argv[1];
$services = $argv[2];

$services = explode(',', $services);

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

function runTask (\Updashd\Scheduler\Model\Task $task) {
    printf('Consuming % 4d %-10s', $task->getId(), $task->getService());
    
    for ($i = 0; $i < 10; $i++) {
        echo '#';
        flush();
        usleep(100000);
    }
    
    echo ' Done';
    echo PHP_EOL;
}

while ($task = $scheduler->getTask($services)) {
    $config = $task->getConfig();
    
    try {
        $config = json_decode($config, true);
        
        if ($config === NULL) {
            throw new \Updashd\Scheduler\Exception\InvalidConfigurationException('JSON could not be decoded: ' . json_last_error_msg(), json_last_error());
        }
    
        if (! isset($config['value'])) {
            throw new \Updashd\Scheduler\Exception\InvalidConfigurationException('Missing configuration value');
        }
    
        $result = $scheduler->markProcessing($task);
    
        // Do "work"
        runTask($task);
    
        $result->setResult(json_encode([
            'done' => true
        ]));
    
        $scheduler->recordResult($result);
    
        $scheduler->markComplete($task);
    }
    catch (Exception $e) {
        $scheduler->createAndRecordError($task->getId(), $e->getCode(), $e->getMessage());
    }
}
