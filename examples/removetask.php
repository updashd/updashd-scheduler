<?php
require 'bootstrap.php';

if ($argc < 3) {
    echo 'Usage: ' . $argv[0] . ' <zone> <taskId>';
    die(1);
}

$zone = $argv[1];
$taskId = $argv[2];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

$task = $scheduler->getTaskEntity($taskId);

$result = $scheduler->removeTask($taskId);

if ($result) {
    echo 'ID: ' . $taskId . PHP_EOL;
    echo 'State: ' . $task->getState() . PHP_EOL;
    echo 'Run Count: ' . $task->getRunCount() . PHP_EOL;
}
else {
    echo 'FAILED!!!  ' . $taskId . PHP_EOL;
}


