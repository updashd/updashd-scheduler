<?php
require 'bootstrap.php';

if ($argc < 6) {
    echo 'Usage: ' . $argv[0] . ' <zone> <service> <accountId> <once|cron|second_interval> <value>';
    die(1);
}

$zone = $argv[1];
$service = $argv[2];
$accountId = $argv[3];
$type = $argv[4];
$value = $argv[5];

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

$taskId = rand(1, 9999);
$task = $scheduler->createTask($taskId);

switch ($type) {
    case 'cron':
        $task->setScheduleType(\Updashd\Scheduler\ScheduleType::TYPE_CRON);
        $task->setCron($value);
        break;
    case 'second_interval':
        $task->setScheduleType(\Updashd\Scheduler\ScheduleType::TYPE_SECOND_INTERVAL);
        $task->setInterval($value);
        break;
    case 'once':
        $task->setScheduleType(\Updashd\Scheduler\ScheduleType::TYPE_ONCE);
        break;
    default:
        die('Unknown run type: ' . $type);
        break;
}

$task->setConfig(json_encode([
    'value' => 'blah'
]));

$task->setService($service);

$task->setAccountId($accountId);

$scheduler->addTask($task);

echo 'Zone: ' . $zone . PHP_EOL;
echo 'Type: ' . $type . PHP_EOL;
echo 'ID: ' . $task->getId() . PHP_EOL;
echo 'Service: ' . $task->getService() . PHP_EOL;

