<?php
require 'bootstrap.php';

if ($argc < 3) {
    echo 'Usage: ' . $argv[0] . ' <zone> <taskIds>' . PHP_EOL;
    echo 'Services is a comma separated variable of the list of taskIds to subscribe to changes for.' . PHP_EOL;
    die(1);
}

$zone = $argv[1];
$taskIds = $argv[2];

$taskIds = explode(',', $taskIds);

$client = new \Predis\Client('tcp://127.0.0.1:6379?read_write_timeout=0');

$scheduler = new \Updashd\Scheduler\Scheduler($client, $zone);

$stateChanges = $scheduler->subscribeStateChange($taskIds);

foreach ($stateChanges as $stateChange) {
    print_r($stateChange);
}