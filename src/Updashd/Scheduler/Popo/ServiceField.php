<?php

namespace Updashd\Scheduler\Popo;

class ServiceField {

    const TYPE_INT = 'INT';
    const TYPE_FLOAT = 'FLOAT';
    const TYPE_STR = 'STR';
    const TYPE_TXT = 'TXT';

    const FIELD_KEY = 'key';
    const FIELD_NAME = 'name';
    const FIELD_TYPE = 'type';
    const FIELD_UNIT = 'unit';

    const KEY_MAX_LENGTH = 30;
    const NAME_MAX_LENGTH = 250;
    const UNIT_MAX_LENGTH = 20;
    const TYPE_MAX_LENGTH = 5;

    public static $TYPES = [
        self::TYPE_INT,
        self::TYPE_FLOAT,
        self::TYPE_STR,
        self::TYPE_TXT
    ];

    /** @var string $key The programmatic name of the key to use for storage/retrieval */
    protected $key;

    /** @var string $name The readable name to use for the name of the metric */
    protected $name;

    /** @var string $type One of the self::TYPE_* constants */
    protected $type;

    /** @var string $unit The unit of the metric (eg. secs) */
    protected $unit;

    /**
     * ServiceMetricField constructor.
     *
     * @param string $key The programmatic name of the key to use for storage/retrieval
     * @param string $name The readable name to use for the name of the metric
     * @param string $type One of the self::TYPE_* constants
     * @param string $unit The unit of the metric (eg. secs)
     */
    public function __construct (string $key, string $name, string $type = self::TYPE_STR, string $unit = '') {
        $this->setKey($key);
        $this->setName($name);
        $this->setType($type);
        $this->setUnit($unit);
    }

    /**
     * @return string
     */
    public function getKey () : string {
        return $this->key;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey (string $key) : ServiceField {
        if (strlen($key) == 0) {
            throw new \InvalidArgumentException('Key must be a non-empty string');
        }

        if (strlen($key) > self::KEY_MAX_LENGTH) {
            throw new \InvalidArgumentException('Key must be less than ' . self::KEY_MAX_LENGTH . ' characters.');
        }

        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getName () : string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName (string $name) : ServiceField {
        if (strlen($name) == 0) {
            throw new \InvalidArgumentException('Name must be a non-empty string');
        }

        if (strlen($name) > self::NAME_MAX_LENGTH) {
            throw new \InvalidArgumentException('Name must be less than ' . self::NAME_MAX_LENGTH . ' characters.');
        }

        $this->name = $name;

        return $this;
    }

    /**
     * @return string one of ServiceMetricField::TYPE_*
     */
    public function getType () : string {
        return $this->type;
    }

    /**
     * @param string $type one of ServiceMetricField::TYPE_*
     * @return ServiceField
     */
    public function setType (string $type) : ServiceField {
        if (strlen($type) > self::TYPE_MAX_LENGTH) {
            throw new \InvalidArgumentException('Type must be less than ' . self::TYPE_MAX_LENGTH . ' characters.');
        }

        if (! in_array($type, self::$TYPES)) {
            throw  new \InvalidArgumentException('Type must be one of the TYPE_ constants');
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnit () : string {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return ServiceField
     */
    public function setUnit ($unit) : ServiceField {
        if (strlen($unit) > self::UNIT_MAX_LENGTH) {
            throw new \InvalidArgumentException('Unit must be less than ' . self::UNIT_MAX_LENGTH . ' characters.');
        }

        $this->unit = $unit;

        return $this;
    }

    /**
     * Serialize the object to array form.
     *
     * @return array
     */
    public function toArray () : array {
        return [
            self::FIELD_KEY => $this->getKey(),
            self::FIELD_NAME => $this->getName(),
            self::FIELD_TYPE => $this->getType(),
            self::FIELD_UNIT => $this->getUnit()
        ];
    }

    /**
     * Populate the object from its array representation
     *
     * @param array $input
     * @return self
     */
    public static function fromArray (array $input) {
        if (array_keys($input) != [self::FIELD_KEY, self::FIELD_NAME, self::FIELD_TYPE, self::FIELD_UNIT]) {
            throw new \InvalidArgumentException('Cannot create from array: one or more fields missing.');
        }

        return new self(
            $input[self::FIELD_KEY],
            $input[self::FIELD_NAME],
            $input[self::FIELD_TYPE],
            $input[self::FIELD_UNIT]
        );
    }
}