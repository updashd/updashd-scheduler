<?php

namespace Updashd\Scheduler\Popo;

class Service {

    const FIELD_MODULE_NAME = 'module_name';
    const FIELD_READABLE_NAME = 'readable_name';
    const FIELD_FIELDS = 'fields';

    const MODULE_NAME_MAX_LENGTH = 20;
    const READABLE_NAME_MAX_LENGTH = 50;

    protected $moduleName;
    protected $readableName;
    protected $fields = [];

    public function __construct (string $moduleName, string $readableName) {
        $this->setModuleName($moduleName);
        $this->setReadableName($readableName);
    }

    /**
     * @return ServiceField[]
     */
    public function getFields () : array {
        return $this->fields;
    }

    /**
     * @param ServiceField[] $fields
     */
    public function setFields (array $fields) {
        foreach ($fields as $field) {
            if (! $field instanceof ServiceField) {
                throw new \InvalidArgumentException(
                    'setFields was given an array that contains at least ' .
                    'one element that is not an instance of ServiceField'
                );
            }
        }

        $this->fields = $fields;
    }

    /**
     * Get the fields as a simple array.
     *
     * @return array
     */
    public function getFieldsAsArray () : array {
        $out = [];

        foreach ($this->getFields() as $field) {
            $out[] = $field->toArray();
        }

        return $out;
    }

    /**
     * Populate the fields from a simple array
     *
     * @param array $input
     */
    public function setFieldsFromArray (array $input) {
        foreach ($input as $field) {
            $this->addField(ServiceField::fromArray($field));
        }
    }

    /**
     * @param ServiceField $field
     * @return ServiceField
     */
    public function addField (ServiceField $field) : ServiceField {
        $this->fields[] = $field;

        return $field;
    }

    /**
     * Add a field quickly from a key, name, type and unit.
     *
     * @param string $key
     * @param string $name
     * @param string $type ServiceMetricField::TYPE_*
     * @param string $unit
     * @return ServiceField
     */
    public function addFieldQuick (string $key, string $name, string $type = ServiceField::TYPE_STR, string $unit = '') : ServiceField {
        return $this->addField(new ServiceField($key, $name, $type, $unit));
    }

    /**
     * Add an INT field.
     *
     * @param string $key
     * @param string $name
     * @param string $unit
     * @return ServiceField
     */
    public function addFieldInt (string $key, string $name, string $unit = '') : ServiceField {
        return $this->addFieldQuick($key, $name, ServiceField::TYPE_INT, $unit);
    }

    /**
     * Add a float field.
     *
     * @param string $key
     * @param string $name
     * @param string $unit
     * @return ServiceField
     */
    public function addFieldFloat (string $key, string $name, $unit = '') : ServiceField {
        return $this->addFieldQuick($key, $name, ServiceField::TYPE_FLOAT, $unit);
    }


    /**
     * Add a string field.
     *
     * @param string $key
     * @param string $name
     * @param string $unit
     * @return ServiceField
     */
    public function addFieldStr (string $key, string $name, string $unit = '') : ServiceField {
        return $this->addFieldQuick($key, $name, ServiceField::TYPE_STR, $unit);
    }


    /**
     * Add a TXT field.
     *
     * @param string $key
     * @param string $name
     * @param string $unit
     * @return ServiceField
     */
    public function addFieldTxt (string $key, string $name, string $unit = '') : ServiceField {
        return $this->addFieldQuick($key, $name, ServiceField::TYPE_TXT, $unit);
    }

    /**
     * Convert to an array for serialization
     *
     * @return array
     */
    public function toArray () {
        return [
            self::FIELD_MODULE_NAME => $this->getModuleName(),
            self::FIELD_READABLE_NAME => $this->getReadableName(),
            self::FIELD_FIELDS => $this->getFieldsAsArray()
        ];
    }

    /**
     * Read from serialized array
     *
     * @param array $input
     * @return \Updashd\Scheduler\Popo\Service
     */
    public static function fromArray (array $input) {
        if (array_diff([self::FIELD_MODULE_NAME, self::FIELD_READABLE_NAME], array_keys($input)) != []) {
            throw new \InvalidArgumentException('Cannot create from array: one or more fields missing.');
        }

        $service = new self($input[self::FIELD_MODULE_NAME], $input[self::FIELD_READABLE_NAME]);

        if (isset($input[self::FIELD_FIELDS])) {
            if (! is_array($input[self::FIELD_FIELDS])) {
                throw new \InvalidArgumentException('Fields is set, but is not an array.');
            }

            $service->setFieldsFromArray($input[self::FIELD_FIELDS]);
        }

        return $service;
    }

    /**
     * Get JSON representation of object
     *
     * @return string
     */
    public function toJson () {
        return json_encode($this->toArray(), JSON_NUMERIC_CHECK);
    }

    /**
     * Populate object from JSON representation of object
     *
     * @param string $jsonString
     * @return \Updashd\Scheduler\Popo\Service
     */
    public static function fromJson (string $jsonString) {
        return self::fromArray(json_decode($jsonString, true));
    }

    /**
     * @return string
     */
    public function getModuleName () : string {
        return $this->moduleName;
    }

    /**
     * @param string $moduleName
     */
    public function setModuleName (string $moduleName) {
        if (strlen($moduleName) == 0) {
            throw new \InvalidArgumentException('moduleName must be a non-empty string');
        }

        if (strlen($moduleName) >= self::MODULE_NAME_MAX_LENGTH) {
            throw new \InvalidArgumentException('moduleName must be less than ' . self::MODULE_NAME_MAX_LENGTH . ' characters.');
        }

        $this->moduleName = $moduleName;
    }

    /**
     * @return string
     */
    public function getReadableName () : string {
        return $this->readableName;
    }

    /**
     * @param string $readableName
     */
    public function setReadableName (string $readableName) {
        if (strlen($readableName) == 0) {
            throw new \InvalidArgumentException('readableName must be a non-empty string');
        }

        if (strlen($readableName) >= self::READABLE_NAME_MAX_LENGTH) {
            throw new \InvalidArgumentException('readableName must be less than ' . self::READABLE_NAME_MAX_LENGTH . ' characters.');
        }

        $this->readableName = $readableName;
    }
}