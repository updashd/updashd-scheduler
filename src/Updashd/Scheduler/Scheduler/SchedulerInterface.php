<?php
namespace Updashd\Scheduler\Scheduler;

use Predis\PubSub\AbstractConsumer;
use Updashd\Scheduler\Model\Error;
use Updashd\Scheduler\Model\Result;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Popo\Service;

interface SchedulerInterface {

    /**
     * Create a task model for the zone and return it.
     *
     * @param string $taskId
     * @return Task
     */
    public function createTask (string $taskId) : Task;

    /**
     * Create a result model.
     *
     * @param string $taskIdOrResultId
     * @param int|null $timestamp Unix timestamp
     * @return Result
     */
    public function createResult (string $taskIdOrResultId, int $timestamp = null) : Result;

    /**
     * Create an error model
     *
     * @param string $taskId
     * @return Error
     */
    public function createError (string $taskId) : Error;

    /**
     * Queue up a service to be registered.
     *
     * @param Service $serviceMetricFields
     */
    public function registerService (Service $serviceMetricFields) : void;

    /**
     * Add a task to the scheduler.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function addTask (Task $task) : void;

    /**
     * Get a task from the queue. BLOCKING!
     *
     * @param string|string[] $services Service module names to process the queues for.
     * @return Task
     */
    public function getTask ($services) : Task;

    /**
     * @param string|string[] $services
     * @return \Updashd\Scheduler\Model\Task|null
     */
    public function getTaskNonBlocking (array $services) : ?Task;

    /**
     * Get a service registration from the queue. BLOCKING!
     *
     * @return Service
     */
    public function getServiceRegistrationBlocking () : Service;

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @deprecated
     * @param int $timeout in seconds
     * @return bool|Service Service if one is ready, false otherwise.
     */
    public function getServiceRegistrationQuick (int $timeout = 2);

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @param int $timeout in seconds
     * @return null|Service Service if one is ready, null otherwise.
     */
    public function getServiceRegistrationOrTimeout (int $timeout = 2) : ?Service;

    /**
     * Mark a task as being processed.
     *
     * @param Task $task
     * @return Result
     * @throws \Exception
     */
    public function markProcessing (Task $task) : Result;

    /**
     * Mark a task as being complete.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function markComplete (Task $task) : void;

    /**
     * Record the result of a task.
     *
     * @param Result $result
     * @throws \Exception
     */
    public function recordResult (Result $result) : void;

    /**
     * Create and record an error.
     *
     * @param string $id
     * @param string $code Error code
     * @param string $details Details of the error
     */
    public function createAndRecordError (string $id, string $code, string $details) : void;

    /**
     * Record an error.
     *
     * @param Error $error
     */
    public function recordError (Error $error) : void;

    /**
     * Push an incident ID to the incident queue.
     *
     * @param string $incidentId
     */
    public function pushIncident (string $incidentId) : void;

    /**
     * Remove a task from the scheduler. This includes removing it from whatever queue it is in according to it's status.
     *
     * @param string $taskId
     * @return bool true if successful, false otherwise
     */
    public function removeTask (string $taskId) : bool;

    /**
     * Determine if there is a task for a given id
     *
     * @param string $taskId
     * @return bool
     */
    public function hasTask (string $taskId) : bool;

    /**
     * Publish a result to be consumed by event subscribers.
     *
     * @param Result $result
     */
    public function publishResult (Result $result) : void;

    /**
     * Get an incident from the queue. BLOCKING!
     *
     * @return int|null
     */
    public function getIncident () : ?int;

    /**
     * Subscribe to results from the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeResult (array $taskIds) : ?AbstractConsumer;

    /**
     * Subscribe to changes in state for the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeStateChange ($taskIds) : ?AbstractConsumer;

    /**
     * Process the schedule. Move tasks that are scheduled to be run into the pending queue.
     *
     * @param int $timestamp the current unix timestamp
     * @return int the count of how many tasks were queued up
     */
    public function processSchedule (int $timestamp) : int;

    /**
     * Process rescheduling tasks
     *
     * @param bool $forever
     * @param int $timeout
     */
    public function processReschedule (bool $forever = true, int $timeout = 30) : void;

    /**
     * Move items from the processing queue back into the pending queue if they have not been completed.
     *
     * @param int $timestamp the current timestamp
     * @param int $zombieTimeout the number of seconds a task can sit before being considered a zombie.
     * @return int
     */
    public function processZombies (int $timestamp, int $zombieTimeout = 30) : int;

    /**
     * @return null|Error|Result
     */
    public function getResultOrError ();

    /**
     * @param int $timeout
     * @return null|Error|Result
     */
    public function getResultOrErrorQuick (int $timeout = 2);

    /**
     * Mark a result as recorded. This should be called from the recorder only.
     *
     * @param Result $result
     */
    public function markResultRecorded (Result $result) : void;

    /**
     * Mark a error as recorded. This should be called from the recorder only.
     *
     * @param Error $error
     */
    public function markErrorReported (Error $error) : void;

    public function getRescheduleSize () : int;

    public function getScheduleSize () : int;

    public function getPendingSize ($service) : int;

    public function getProcessingSize () : int;

    public function getCompleteSize () : int;

    public function getErrorSize () : int;

    public function getIncidentSize () : int;

    public function getRegisterFieldSize () : int;

    public function startTransaction () : void;

    public function commitTransaction () : void;

    public function discardTransaction () : void;

    public function getTaskEntity (string $taskId, bool $retrieveNow = true) : Task;

    public function getResultEntity (string $resultId, bool $retrieveNow = true) : Result;

    public function getErrorEntity (string $taskId, bool $retrieveNow = true) : Error;

}