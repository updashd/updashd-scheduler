<?php

namespace Updashd\Scheduler\Scheduler;

use Exception;
use G4\Cron\CronExpression;
use InvalidArgumentException;
use Updashd\Scheduler\Communication\ClientInterface;
use Predis\PubSub\AbstractConsumer;
use Updashd\Scheduler\Exception\InvalidConfigurationException;
use Updashd\Scheduler\Model\Error;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Model\Result;
use Updashd\Scheduler\Popo\Service;
use Updashd\Scheduler\Queue;
use Updashd\Scheduler\ScheduleType;
use Updashd\Scheduler\State;

class SchedulerImpl implements SchedulerInterface {

    const RESULT_EXPIRE_TIME = 30; // In seconds
    const ERROR_EXPIRE_TIME = 30; // In seconds
    const TASK_EXPIRE_TIME = 30; // In seconds

    const CHANNEL_STATE_CHANGE_PREFIX = 'state-change::';
    const CHANNEL_RESULT_PREFIX = 'result::';

    const ZONE_MAX_LENGTH = 10;

    /** @var string $zone the programmatic name of the zone */
    private $zone;

    /** @var ClientInterface $client the client to use to interact with redis */
    private $client;

    /**
     * Scheduler constructor.
     *
     * @param \Updashd\Scheduler\Communication\ClientInterface $client
     * @param string $zone
     */
    public function __construct (ClientInterface $client, string $zone) {
        $this->setClient($client);
        $this->setZone($zone);
    }

    /**
     * Create a task model for the zone and return it.
     *
     * @param string $taskId
     * @return Task
     */
    public function createTask (string $taskId) : Task {
        if (! $taskId) {
            throw new InvalidArgumentException('Task ID must be non-empty');
        }

        return new Task($this->getClient(), $this->getZone(), $taskId);
    }

    /**
     * Create a result model.
     *
     * @param string $taskIdOrResultId
     * @param int|null $timestamp Unix timestamp
     * @return Result
     */
    public function createResult (string $taskIdOrResultId, int $timestamp = null) : Result {
        if (! $taskIdOrResultId) {
            throw new InvalidArgumentException('taskIdOrResultId must be non-empty');
        }

        return new Result($this->getClient(), $this->getZone(), $taskIdOrResultId, $timestamp);
    }

    /**
     * Create an error model
     *
     * @param string $taskId
     * @return Error
     */
    public function createError (string $taskId) : Error {
        if (! $taskId) {
            throw new InvalidArgumentException('Task ID must be non-empty');
        }

        return new Error($this->getClient(), $this->getZone(), $taskId);
    }

    /**
     * Queue up a service to be registered.
     *
     * @param Service $serviceMetricFields
     */
    public function registerService (Service $serviceMetricFields) : void {
        $this->pushRegisterField($serviceMetricFields->toJson());
    }

    /**
     * Add a task to the scheduler.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function addTask (Task $task) : void {
        try {
            $this->startTransaction();

            $task->setLastCompletionTime(null);
            $task->setRunCount(0);
            $task->save();
            $this->pushReschedule($task->getId());

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
            throw $e;
        }
    }

    /**
     * Get a task from the queue. BLOCKING!
     *
     * @param string|string[] $services Service module names to process the queues for.
     * @return Task
     */
    public function getTask ($services) : Task {
        do {
            $taskId = $this->popPending($services);
        }
        while (! $taskId);

        return $this->getTaskEntity($taskId);
    }

    /**
     * @param string|string[] $services
     * @return \Updashd\Scheduler\Model\Task|null
     */
    public function getTaskNonBlocking (array $services) : ?Task {
        if ($taskId = $this->popPending($services, 1)) { // Timeout of 1 here because there is no truly non-blocking BRPOP
            return $this->getTaskEntity($taskId);
        }

        return null;
    }

    /**
     * Get a service registration from the queue. BLOCKING!
     *
     * @return Service
     */
    public function getServiceRegistrationBlocking () : Service {
        do {
            $fieldJson = $this->popServiceRegister();
        }
        while (! $fieldJson);

        return Service::fromJson($fieldJson);
    }

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @deprecated
     * @param int $timeout in seconds
     * @return bool|Service Service if one is ready, false otherwise.
     */
    public function getServiceRegistrationQuick (int $timeout = 2) {
        return $this->getServiceRegistrationOrTimeout($timeout) ?: false;
    }

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @param int $timeout in seconds
     * @return null|Service Service if one is ready, null otherwise.
     */
    public function getServiceRegistrationOrTimeout (int $timeout = 2) : ?Service {
        $fieldJson = $this->popServiceRegister($timeout);

        if ($fieldJson) {
            return Service::fromJson($fieldJson);
        }

        return null;
    }

    /**
     * Mark a task as being processed.
     *
     * @param Task $task
     * @return Result
     * @throws \Exception
     */
    public function markProcessing (Task $task) : Result {
        try {
            $this->startTransaction();

            $this->pushProcessing($task->getId());

            $result = $this->createResult($task->getId(), $task->getScheduledTime());

            // Set the task id
            $result->setTaskId($task->getId());

            // Record the start time
            $result->setStartTime(time());

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
            throw $e;
        }

        return $result;
    }

    /**
     * Mark a task as being complete.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function markComplete (Task $task) : void {
        try {
            $this->startTransaction();

            $this->pushReschedule($task->getId());
            $this->removeProcessing($task->getId());

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
            throw $e;
        }
    }

    /**
     * Record the result of a task.
     *
     * @param Result $result
     * @throws \Exception
     */
    public function recordResult (Result $result) : void {
        // Record the end time
        $result->setEndTime(time());

        try {
            $this->startTransaction();

            // Save to Redis
            $result->save();
            $this->pushComplete($result->getTaskId(), $result->getId());

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
            throw $e;
        }
    }

    /**
     * Create and record an error.
     *
     * @param string $id
     * @param string $code Error code
     * @param string $details Details of the error
     */
    public function createAndRecordError (string $id, string $code, string $details) : void {
        $error = $this->createError($id);
        $error->setDate(time());
        $error->setErrorCode($code);
        $error->setErrorDetails($details);

        $this->recordError($error);
    }

    /**
     * Record an error.
     *
     * @param Error $error
     */
    public function recordError (Error $error) : void {
        try {
            $this->startTransaction();

            $error->save();
            $this->pushError($error->getId(), $error->getId());

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
        }
    }

    /**
     * Push an incident ID to the incident queue.
     *
     * @param string $incidentId
     */
    public function pushIncident (string $incidentId) : void {
        $client = $this->getClient();

        // Push to completed queue
        $client->lpush(Queue::getIncidentKeyName($this->getZone()), $incidentId);
    }

    /**
     * Pop an incident from the queue.
     *
     * @param int $timeout timeout in seconds
     * @return int|null string of incident ID if successful, null otherwise
     */
    private function popIncident (int $timeout = 30) : ?int {
        $client = $this->getClient();

        $result = $client->brpop(Queue::getIncidentKeyName($this->getZone()), $timeout);

        return $result ? $result[1] : null;
    }

    /**
     * Remove a task from the scheduler. This includes removing it from whatever queue it is in according to it's status.
     *
     * @param string $taskId
     * @return bool true if successful, false otherwise
     */
    public function removeTask (string $taskId) : bool {
        $client = $this->getClient();

        $task = $this->getTaskEntity($taskId);

        if (! $task) {
            return false;
        }

        $success = false;

        try {
            $this->startTransaction();

            // Clean up the hashes by setting it to expire in 30 seconds
            $client->expire(Task::getTaskKeyName($this->getZone(), $taskId), self::TASK_EXPIRE_TIME);

            switch ($task->getState()) {
                case State::STATE_PENDING:
                    $success = $this->removePending($taskId);
                    break;
                case State::STATE_SCHEDULED:
                    $success = $this->removeSchedule($taskId);
                    break;
                case State::STATE_RESCHEDULING:
                    $success = $this->removeReschedule($taskId);
                    break;
                case State::STATE_PROCESSING:
                    $success = $this->removeProcessing($taskId);
                    break;
                case State::STATE_COMPLETE:
                    $success = $this->removeComplete($taskId);
                    break;
                case State::STATE_ERROR:
                    $success = $this->removeComplete($taskId);
                    break;
            }

            // Set the state to disabled
            $this->setTaskState($taskId, State::STATE_DISABLED);

            $this->commitTransaction();
        }
        catch (Exception $e) {
            $this->discardTransaction();
        }

        return $success;
    }

    /**
     * Determine if there is a task for a given id
     *
     * @param string $taskId
     * @return bool
     */
    public function hasTask (string $taskId) : bool {
        $client = $this->getClient();

        // True if the state field exists, false otherwise
        return $client->hlen(Task::getTaskKeyName($this->getZone(), $taskId)) > 0;
    }

    /**
     * Publish a result to be consumed by event subscribers.
     *
     * @param Result $result
     */
    public function publishResult (Result $result) : void {
        $client = $this->getClient();
        $client->publish(self::CHANNEL_RESULT_PREFIX . Task::getTaskKeyName($this->getZone(), $result->getTaskId()), $result->getId());
    }

    /**
     * Get an incident from the queue. BLOCKING!
     *
     * @param int $timeout
     * @return int|null
     */
    public function getIncident (int $timeout = 30) : ?int {
        do {
            $incidentId = $this->popIncident($timeout);
        }
        while (! $incidentId);

        return $incidentId;
    }

    /**
     * Subscribe to results from the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeResult (array $taskIds) : ?AbstractConsumer {
        $client = $this->getClient();
        $pubSub = $client->pubSubLoop();

        $realTaskIds = [];

        foreach ($taskIds as $taskId) {
            $realTaskIds[] = self::CHANNEL_RESULT_PREFIX . Task::getTaskKeyName($this->getZone(), $taskId);
        }

        $pubSub->subscribe($realTaskIds);

        return $pubSub;
    }

    /**
     * Subscribe to changes in state for the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeStateChange ($taskIds) : ?AbstractConsumer {
        $client = $this->getClient();
        $pubSub = $client->pubSubLoop();

        $realTaskIds = [];

        foreach ($taskIds as $taskId) {
            $realTaskIds[] = self::CHANNEL_STATE_CHANGE_PREFIX . Task::getTaskKeyName($this->getZone(), $taskId);
        }

        $pubSub->subscribe($realTaskIds);

        return $pubSub;
    }

    /**
     * Process the schedule. Move tasks that are scheduled to be run into the pending queue.
     *
     * @param int $timestamp the current unix timestamp
     * @return int the count of how many tasks were queued up
     */
    public function processSchedule (int $timestamp) : int {
        $client = $this->getClient();

        $scheduleQueue = Queue::getScheduleKeyName($this->getZone());

        // Get all tasks due
        $tasks = $client->zrevrangebyscore($scheduleQueue, $timestamp, 0);

        foreach ($tasks as $taskId) {
            // Add to pending queue
            $this->pushPending($taskId);

            // Remove from sorted set
            $client->zrem($scheduleQueue, $taskId);
        }

        return count($tasks);
    }

    /**
     * Process rescheduling tasks
     *
     * @param bool $forever
     * @param int $timeout
     */
    public function processReschedule (bool $forever = true, int $timeout = 30) : void {
        do {
            $taskId = $this->popReschedule($timeout);

            if (! $taskId) {
                continue;
            }

            $task = $this->getTaskEntity($taskId);

            try {

                if ($task->getEnabled()) {
                    $type = $task->getScheduleType();

                    $timestamp = time();
                    $run = true;

                    switch ($type) {
                        case ScheduleType::TYPE_ONCE:
                            if ($task->getRunCount() > 0) {
                                $run = false;
                            }
                            else {
                                // Schedule Immediately
                                $timestamp = time();
                            }
                            break;
                        case ScheduleType::TYPE_CRON:
                            try {
                                $expression = CronExpression::factory($task->getCron());
                                $timestamp = $expression->getNextRunDate()->getTimestamp();
                            }
                            catch (Exception $e) {
                                throw new InvalidConfigurationException('Invalid cron expression', null, $e);
                            }

                            break;
                        case ScheduleType::TYPE_SECOND_INTERVAL:
                            if (! is_numeric($task->getInterval())) {
                                throw new InvalidConfigurationException('Interval for schedule is not an integer');
                            }

                            $timestamp = strtotime('+' . $task->getInterval() . ' sec');
                            break;
                    }

                    if ($run) {
                        $this->pushSchedule($timestamp, $taskId);
                    }
                }
            }
            catch (Exception $e) {
                $error = $this->createError($taskId);
                $error->setTaskId($taskId);
                $error->setDate(time());
                $error->setErrorCode($e->getCode());
                $error->setErrorDetails($e->getMessage());
                $this->recordError($error);
            }
        }
        while($forever);
    }

    /**
     * Move items from the processing queue back into the pending queue if they have not been completed.
     *
     * @param int $timestamp the current timestamp
     * @param int $zombieTimeout the number of seconds a task can sit before being considered a zombie.
     * @return int
     */
    public function processZombies (int $timestamp, int $zombieTimeout = 30) : int {
        $client = $this->getClient();

        $processingQueue = Queue::getProcessingKeyName($this->getZone());

        // Get all tasks due 30 seconds or more ago
        $tasks = $client->zrevrangebyscore($processingQueue, $timestamp - $zombieTimeout, 0);

        foreach ($tasks as $taskId) {
            // Add to pending queue
            $this->pushPending($taskId);

            // Remove from sorted set
            $client->zrem($processingQueue, $taskId);
        }

        return count($tasks);
    }

    /**
     * @return null|Error|Result
     */
    public function getResultOrError () {
        do {
            $result = $this->popCompleteOrError();
        }
        while (! $result);

        list ($type, $id) = $result;

        if ($type == 'error') {
            return $this->getErrorEntity($id);
        }
        elseif ($type == 'result') {
            return $this->getResultEntity($id);
        }

        return null;
    }

    /**
     * @param int $timeout
     * @return null|Error|Result
     */
    public function getResultOrErrorQuick (int $timeout = 2) {
        $result = $this->popCompleteOrError($timeout);

        if ($result) {
            return $this->extractResultOrError($result);
        }

        return null;
    }

    /**
     * Mark a result as recorded. This should be called from the recorder only.
     *
     * @param Result $result
     */
    public function markResultRecorded (Result $result) : void {
        $this->getClient()->expire(Result::getResultKeyNameFromResultId($this->getZone(), $result->getId()), self::RESULT_EXPIRE_TIME);
    }

    /**
     * Mark a error as recorded. This should be called from the recorder only.
     *
     * @param Error $error
     */
    public function markErrorReported (Error $error) : void {
        $this->getClient()->expire(Error::getErrorKeyName($this->getZone(), $error->getId()), self::ERROR_EXPIRE_TIME);
    }

    public function getRescheduleSize () : int {
        return $this->getClient()->llen(Queue::getRescheduleKeyName($this->getZone()));
    }

    public function getScheduleSize () : int {
        return $this->getClient()->zcard(Queue::getScheduleKeyName($this->getZone()));
    }

    public function getPendingSize ($service) : int {
        return $this->getClient()->llen(Queue::getPendingKeyName($this->getZone(), $service));
    }

    public function getProcessingSize () : int {
        return $this->getClient()->zcard(Queue::getProcessingKeyName($this->getZone()));
    }

    public function getCompleteSize () : int {
        return $this->getClient()->llen(Queue::getCompleteKeyName($this->getZone()));
    }

    public function getErrorSize () : int {
        return $this->getClient()->llen(Queue::getErrorKeyName($this->getZone()));
    }

    public function getIncidentSize () : int {
        return $this->getClient()->llen(Queue::getIncidentKeyName($this->getZone()));
    }

    public function getRegisterFieldSize () : int {
        return $this->getClient()->llen(Queue::getServiceRegisterKeyName($this->getZone()));
    }

    public function startTransaction () : void {
        $this->getClient()->multi();
    }

    public function commitTransaction () : void {
        $this->getClient()->exec();
    }

    public function discardTransaction () : void {
        $this->getClient()->discard();
    }

    public function getTaskEntity (string $taskId, bool $retrieveNow = true) : Task {
        $task = $this->createTask($taskId);

        if ($retrieveNow) {
            $task->retrieve();
        }

        return $task;
    }

    public function getResultEntity (string $resultId, bool $retrieveNow = true) : Result {
        $result = $this->createResult($resultId);

        if ($retrieveNow) {
            $result->retrieve();
        }

        return $result;
    }

    public function getErrorEntity (string $taskId, bool $retrieveNow = true) : Error {
        $error = $this->createError($taskId);

        if ($retrieveNow) {
            $error->retrieve();
        }

        return $error;
    }

    private function setTaskState (string $taskId, string $state) : void {
        if (! State::isState($state)) {
            throw new InvalidArgumentException('State "' . $state . '" is not a valid state.');
        }

        $client = $this->getClient();

        $taskHashKey = Task::getTaskKeyName($this->getZone(), $taskId);

        // Set the state on the task
        $client->hset($taskHashKey, Task::FIELD_STATE, $state);

        // If the state is complete, set the previous run time
        if ($state == State::STATE_COMPLETE) {
            $client->hset($taskHashKey, Task::FIELD_LAST_COMPLETION_TIME, time());
        }

        // Publish the state change
        $client->publish(self::CHANNEL_STATE_CHANGE_PREFIX . $taskHashKey, $state);
    }

    private function pushReschedule (string $taskId) : void {
        $client = $this->getClient();

        // Clear any expiration dates if the task is enabled before expiring
        $client->persist(Task::getTaskKeyName($this->getZone(), $taskId));

        // Push to reschedule queue
        $client->lpush(Queue::getRescheduleKeyName($this->getZone()), $taskId);

        $this->setTaskState($taskId, State::STATE_RESCHEDULING);
    }

    private function pushSchedule (int $timestamp, string $taskId) : void {
        $client = $this->getClient();

        $hashKey = Task::getTaskKeyName($this->getZone(), $taskId);

        // Update the scheduled time
        $client->hset($hashKey, Task::FIELD_SCHEDULED_TIME, $timestamp);

        // Add to schedule sorted set
        $client->zadd(Queue::getScheduleKeyName($this->getZone()), [$taskId => $timestamp]);

        $this->setTaskState($taskId, State::STATE_SCHEDULED);
    }

    private function pushPending (string  $taskId) : void {
        $client = $this->getClient();

        // This one is actually needed since it gets the service
        $task = $this->getTaskEntity($taskId);

        // Push to pending queue
        $client->lpush(Queue::getPendingKeyName($this->getZone(), $task->getService()), $taskId);

        $this->setTaskState($taskId, State::STATE_PENDING);
    }

    private function pushProcessing (string $taskId) : void {
        $client = $this->getClient();

        $timestamp = time();

        // Push to processing sorted set
        $client->zadd(Queue::getProcessingKeyName($this->getZone()), [$taskId => $timestamp]);

        $this->setTaskState($taskId, State::STATE_PROCESSING);
    }

    private function pushComplete (string $taskId, string $resultId) : void {
        $client = $this->getClient();

        $taskKeyName = Task::getTaskKeyName($this->getZone(), $taskId);

        // Increment run count by one
        $client->hincrby($taskKeyName, Task::FIELD_RUN_COUNT, 1);

        // Push to completed queue
        $client->lpush(Queue::getCompleteKeyName($this->getZone()), $resultId);

        $this->setTaskState($taskId, State::STATE_COMPLETE);
    }

    private function pushError (string $taskId, string $errorId) : void {
        $client = $this->getClient();

        // Push to completed queue
        $client->lpush(Queue::getErrorKeyName($this->getZone()), $errorId);

        $this->setTaskState($taskId, State::STATE_ERROR);
    }

    private function pushRegisterField (string $fieldSpecification) {
        if (! $fieldSpecification) {
            throw new InvalidArgumentException('fieldSpecification must be a non-empty string');
        }

        $client = $this->getClient();

        // Push to pending queue
        $client->lpush(Queue::getServiceRegisterKeyName($this->getZone()), $fieldSpecification);
    }

    private function removeReschedule (string $taskId) : int {
        $client = $this->getClient();

        // Remove 1 item from the reschedule queue that matches
        return $client->lrem(Queue::getRescheduleKeyName($this->getZone()), 1, $taskId);
    }

    private function removeSchedule (string $taskId) : int {
        $client = $this->getClient();

        return $client->zrem(Queue::getScheduleKeyName($this->getZone()), $taskId);
    }

    private function removePending (string $taskId) : int {
        $client = $this->getClient();

        $task = $this->getTaskEntity($taskId);

        // Remove 1 item from the pending queue that matches
        return $client->lrem(Queue::getPendingKeyName($this->getZone(), $task->getService()), 1, $taskId);
    }

    private function removeProcessing (string $taskId) : int {
        $client = $this->getClient();

        // Remove from sorted set
        return $client->zrem(Queue::getProcessingKeyName($this->getZone()), $taskId);
    }

    private function removeComplete (string $taskId) : int {
        $client = $this->getClient();

        // Remove from sorted set
        return $client->zrem(Queue::getCompleteKeyName($this->getZone()), $taskId);
    }

    /**
     * Pop the taskId off of the reschedule queue. Blocking for $timeout seconds.
     *
     * @param int $timeout
     * @return string|null taskId when successful, null when nothing was available
     */
    private function popReschedule (int $timeout = 30) : ?string {
        $client = $this->getClient();

        $result = $client->brpop(Queue::getRescheduleKeyName($this->getZone()), $timeout);

        return $result ? $result[1] : null;
    }

    /**
     * Pop a taskId off of any of the the pending queues given. Blocking for $timeout seconds.
     *
     * @param string|string[] $serviceOrServices the name or names of the services to pop for
     * @param int $timeout
     * @return string|null taskId if a task was found in the given time, null if no task within timeout time
     */
    private function popPending ($serviceOrServices, int $timeout = 30) : ?string {
        if (is_string($serviceOrServices)) {
            $serviceOrServices = array($serviceOrServices);
        }
        elseif (! is_array($serviceOrServices)) {
            throw new InvalidArgumentException('Services must be a string or an array');
        }

        $serviceOrServices = array_map(function ($service) {
            return Queue::getPendingKeyName($this->getZone(), $service);
        }, $serviceOrServices);

        $client = $this->getClient();

        $result = $client->brpop($serviceOrServices, $timeout);

        return $result ? $result[1] : null;
    }

    /**
     * Wait for a service registration. Blocking for $timeout seconds.
     *
     * @param int $timeout seconds til timeout
     * @return string|null JSON string representing the service to be registered, null if none within timeout
     */
    private function popServiceRegister (int $timeout = 30) : ?string {
        $client = $this->getClient();

        $result = $client->brpop(Queue::getServiceRegisterKeyName($this->getZone()), $timeout);

        return $result ? $result[1] : null;
    }

    /**
     * Watch both the completion and error queues. Blocking for $timeout seconds.
     *
     * Returns error if success with the result[0] being the type (result or error) and result[1] begin the id
     *
     * @param int $timeout seconds to wait before timing out.
     * @return array|null
     */
    private function popCompleteOrError (int $timeout = 30) : ?array {
        $client = $this->getClient();

        $queues = [
            'result' => Queue::getCompleteKeyName($this->getZone()),
            'error' => Queue::getErrorKeyName($this->getZone()),
        ];

        $result = $client->brpop($queues, $timeout);

        if ($result) {
            list($type, $value) = $result;

            $index = array_search($type, $queues);

            if ($index !== false) {
                return [$index, $value];
            }
            else {
                return null;
            }
        }

        return null;
    }

    /**
     * @param array $result
     * @return bool|\Updashd\Scheduler\Model\Error|\Updashd\Scheduler\Model\Result
     */
    private function extractResultOrError (array $result) {
        list ($type, $id) = $result;

        if ($type == 'error') {
            return $this->getErrorEntity($id);
        }
        elseif ($type == 'result') {
            return $this->getResultEntity($id);
        }

        return null;
    }

    /**
     * @return \Updashd\Scheduler\Communication\ClientInterface
     */
    public function getClient () : ClientInterface {
        return $this->client;
    }

    /**
     * @param \Updashd\Scheduler\Communication\ClientInterface $client
     */
    public function setClient (ClientInterface $client) {
        $this->client = $client;
    }

    /**
     * @return int|string
     */
    public function getZone () : string {
        return $this->zone;
    }

    /**
     * @param int|string $zone
     */
    public function setZone (string $zone) {
        if (! $zone || strlen($zone) == 0 || strlen($zone) > self::ZONE_MAX_LENGTH) {
            throw new InvalidArgumentException('Zone must be a non-empty string no longer than ' . self::ZONE_MAX_LENGTH . ' characters');
        }

        $this->zone = $zone;
    }
}