<?php
namespace Updashd\Scheduler;

/**
 * Supported schedule types
 *
 * @codeCoverageIgnore
 *
 * @package Updashd\Scheduler
 */
class ScheduleType {
    const TYPE_ONCE = 'once';
    const TYPE_CRON = 'cron';
    const TYPE_SECOND_INTERVAL = 'second_interval';


    public static function getScheduleTypes () : array {
        return [
            self::TYPE_ONCE,
            self::TYPE_CRON,
            self::TYPE_SECOND_INTERVAL,
        ];
    }

    public static function isScheduleType (string $input) : bool {
        return in_array($input, self::getScheduleTypes());
    }

    public static function getName (string $input) : bool {
        $map = [
            self::TYPE_ONCE => 'Once',
            self::TYPE_CRON => 'Cron',
            self::TYPE_SECOND_INTERVAL => 'Second Interval',
        ];

        return array_key_exists($input, $map) ? $map[$input] : null;
    }
}