<?php
namespace Updashd\Scheduler\Model;

use Updashd\Scheduler\Communication\ClientInterface;

class Error extends AbstractHashEntity {
    const NAME = 'error';
    const FIELD_TASK_ID = 'TaskId';
    const FIELD_DATE = 'Date';
    const FIELD_ERROR_CODE = 'ErrorCode';
    const FIELD_ERROR_DETAILS = 'ErrorDetails';
    
    const ID_SEPARATOR = '-';
    
    private $fields = [
        self::FIELD_TASK_ID,
        self::FIELD_DATE,
        self::FIELD_ERROR_CODE,
        self::FIELD_ERROR_DETAILS
    ];
    
    private $defaults = [
        self::FIELD_TASK_ID => null,
        self::FIELD_DATE => null,
        self::FIELD_ERROR_CODE => 100,
        self::FIELD_ERROR_DETAILS => null
    ];
    
    public function __construct (ClientInterface $client, $zone, $taskId) {
        parent::__construct($client, $zone, self::NAME, $taskId, $this->fields, $this->defaults);
    }
    
    public static function getErrorKeyName($zone, $taskId) {
        return parent::getHashKeyName($zone, self::NAME, $taskId);
    }
    
    /**
     * Get date of error occurrence
     * @return int
     */
    public function getDate () {
        return $this->getValue(self::FIELD_DATE);
    }
    
    /**
     * @param int $date UNIX TIMESTAMP
     */
    public function setDate ($date) {
        $this->setValue(self::FIELD_DATE, $date);
    }
    
    /**
     * @return mixed
     */
    public function getTaskId () {
        return $this->getValue(self::FIELD_TASK_ID);
    }
    
    /**
     * @param int $taskId
     */
    public function setTaskId ($taskId) {
        $this->setValue(self::FIELD_TASK_ID, $taskId);
    }
    
    /**
     * @return mixed
     */
    public function getErrorCode () {
        return $this->getValue(self::FIELD_ERROR_CODE);
    }
    
    /**
     * @param int $errorCode
     */
    public function setErrorCode ($errorCode) {
        $this->setValue(self::FIELD_ERROR_CODE, $errorCode);
    }
    
    /**
     * Get the details of the error that occurred
     * @return string
     */
    public function getErrorDetails () {
        return $this->getValue(self::FIELD_ERROR_DETAILS);
    }

    /**
     * Set the details of the error
     * @param string $errorDetails
     */
    public function setErrorDetails ($errorDetails) {
        $this->setValue(self::FIELD_ERROR_DETAILS, $errorDetails);
    }
}