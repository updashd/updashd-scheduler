<?php

namespace Updashd\Scheduler\Model;

use InvalidArgumentException;
use RuntimeException;
use Updashd\Scheduler\Communication\ClientInterface;

abstract class AbstractHashEntity {

    const PROPERTY_DEFAULT_VALUE = 'defaultValue';
    const PROPERTY_ORIGINAL_VALUE = 'originalValue';
    const PROPERTY_CURRENT_VALUE = 'currentValue';
    const PROPERTY_IS_LOADED = 'isLoaded';

    /** @var ClientInterface */
    private $client;

    /** @var string $name entity type name */
    private $name;

    /** @var string $zone zone */
    private $zone;

    /** @var array $fieldMap map of fields */
    private $fieldMap;

    /** @var string $id the entity id */
    private $id;

    /**
     * AbstractHashEntity constructor.
     *
     * @param ClientInterface $client
     * @param string $zone
     * @param string $name
     * @param string|null $id
     * @param array $fields The field names to use
     * @param array $defaults
     */
    public function __construct (ClientInterface $client, string $zone, string $name, string $id = null, array $fields = [], array $defaults = []) {
        $this->setClient($client);
        $this->setZone($zone);
        $this->setName($name);
        $this->setFields($fields, $defaults);
        $this->setId($id);
    }

    /**
     * Determine if the given field is dirty, meaning it has been modified from the persisted
     * version.
     * @param string $field
     * @return bool
     */
    public function isFieldDirty (string $field) : bool {
        return (! $this->isLoaded($field) || $this->getOriginalValue($field) != $this->getValue($field));
    }

    /**
     * Determine if the entity is dirty (AKA any field is dirty)
     * @return bool
     */
    public function isEntityDirty () : bool {
        // Check every field to see if it is dirty
        foreach ($this->fieldMap as $field => $fieldProperties) {
            if ($this->isFieldDirty($field)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve the entity from storage.
     * @return bool true when successful, false otherwise
     */
    public function retrieve () : bool {
        $client = $this->getClient();
        $fieldCount = $client->hlen($this->getKeyName());

        if ($fieldCount <= 0) {
            return false;
        }

        $fieldsToRetrieve = array_keys($this->fieldMap);

        $values = $client->hmget($this->getKeyName(), $fieldsToRetrieve);

        $this->populateFields($fieldsToRetrieve, $values);

        return true;
    }

    /**
     * @param array $fields array of the field names
     * @param array $values matching array of values
     * @throws \RuntimeException
     */
    public function populateFields (array $fields, array $values) : void {
        $combinedSet = array_combine($fields, $values);

        foreach ($combinedSet as $field => $value) {
            $this->setOriginalValue($field, $value);
            $this->setValue($field, $value);
            $this->setIsLoaded($field, true);
        }
    }

    /**
     * Retrieve only the given field from storage.
     * @param string $field
     * @return string|null
     */
    public function retrieveField (string $field) : ?string {
        $client = $this->getClient();
        $value = $client->hget($this->getKeyName(), $field);
        $this->setOriginalValue($field, $value);
        $this->setValue($field, $value);
        $this->setIsLoaded($field, true);

        return $value;
    }

    /**
     * Persist the entity if any changes have been made.
     */
    public function save () : void {
        $fieldsToSave = [];

        foreach ($this->fieldMap as $field => $fieldProperties) {
            if ($this->isFieldDirty($field)) {
                $fieldsToSave[$field] = $this->getValue($field);
            }
        }

        // Bail out if we don't have anything to save
        if (! count($fieldsToSave)) {
            return;
        }

        $client = $this->getClient();
        $client->hmset($this->getKeyName(), $fieldsToSave);

        // Set the original value and say it's loaded now that we have saved
        foreach ($fieldsToSave as $field => $value) {
            $this->setOriginalValue($field, $value);
            $this->setIsLoaded($field, true);
        }
    }


    /**
     * Persist a single field to the data store.
     * @param $field
     */
    public function saveField (string $field) : void {
        if ($this->isFieldDirty($field)) {
            $client = $this->getClient();
            $client->hset($this->getKeyName(), $field, $this->getValue($field));

            // Update original value to the set value
            $this->setOriginalValue($field, $this->getValue($field));

            // Set the field as "loaded"
            $this->setIsLoaded($field, true);
        }
    }

    /**
     * Set the value of a field.
     * @param string $field
     * @param string $value
     */
    public function setValue (string $field, ?string $value) : void {
        $this->setProperty($field, self::PROPERTY_CURRENT_VALUE, $value);
    }

    /**
     * Get the value of the given field, retrieving the record if it has not been loaded
     * yet.
     * @param string $field
     * @return mixed
     */
    public function getValue (string $field) : ?string {
        return $this->getProperty($field, self::PROPERTY_CURRENT_VALUE);
    }

    /**
     * Determine if the given field has been loaded from the persistence layer.
     * @param string $field
     * @return mixed
     */
    protected function isLoaded (string $field) : bool {
        return $this->getProperty($field, self::PROPERTY_IS_LOADED);
    }

    /**
     * Set whether the given field has been loaded.
     *
     * @param string $field
     * @param bool $value
     * @return bool
     */
    protected function setIsLoaded (string $field, bool $value) : bool {
        return $this->setProperty($field, self::PROPERTY_IS_LOADED, $value);
    }

    /**
     * Set the fields that are supported by the entity.
     *
     * @param array $fields
     * @param array $defaults
     */
    protected function setFields (array $fields, array $defaults = []) : void {
        $this->fieldMap = [];

        foreach ($fields as $field) {
            $default = null;

            if (array_key_exists($field, $defaults)) {
                $default = $defaults[$field];
            }

            $this->addField($field, $default);
        }
    }

    /**
     * Get the kinds of fields supported by the entity.
     *
     * @return array
     */
    public function getFields () : array {
        return is_array($this->fieldMap) ? array_keys($this->fieldMap) : [];
    }

    /**
     * Add a field to the entity.
     *
     * @param string $field the name of the field.
     * @param string|null $default the default value for the field.
     */
    protected function addField (string $field, ?string $default = null) : void {
        if (! $this->fieldMap) {
            $this->fieldMap = [];
        }

        if (! array_key_exists($field, $this->fieldMap)) {
            $this->fieldMap[$field] = [];
        }

        $this->setProperty($field, self::PROPERTY_DEFAULT_VALUE, $default);
        $this->setProperty($field, self::PROPERTY_ORIGINAL_VALUE, null);
        $this->setProperty($field, self::PROPERTY_CURRENT_VALUE, $default);
        $this->setProperty($field, self::PROPERTY_IS_LOADED, false);
    }

    /**
     * Get a property for the given field.
     *
     * @param string $field
     * @param string $property
     * @return mixed
     */
    protected function getProperty (string $field, string $property) {
        if (! array_key_exists($field, $this->fieldMap)) {
            throw new RuntimeException('Field was not added to entity');
        }

        if (! array_key_exists($property, $this->fieldMap[$field])) {
            throw new RuntimeException('Property is not defined');
        }

        return $this->fieldMap[$field][$property];
    }

    /**
     * Set a property for the given field.
     * @param string $field
     * @param string $property
     * @param mixed $value
     * @return mixed
     */
    protected function setProperty (string $field, string $property, $value) {
        if (! array_key_exists($field, $this->fieldMap)) {
            throw new RuntimeException('Field was not added to entity');
        }

        return $this->fieldMap[$field][$property] = $value;
    }

    /**
     * Get the original value for the given field (the value loaded before modification)
     *
     * @param string $field
     * @return string|null
     */
    public function getOriginalValue (string $field) : ?string {
        return $this->getProperty($field, self::PROPERTY_ORIGINAL_VALUE);
    }

    /**
     * Set the original value for a field.
     *
     * @param string $field
     * @param string|null $value
     * @return string|null
     */
    protected function setOriginalValue (string $field, ?string $value) : ?string {
        return $this->setProperty($field, self::PROPERTY_ORIGINAL_VALUE, $value);
    }

    /**
     * Get the key name to use for the persistence layer.
     * @return string
     */
    public function getKeyName () : string {
        return self::getHashKeyName($this->getZone(), $this->getName(), $this->getId());
    }

    /**
     * Get the hash key name to use for persisting an entity.
     *
     * @param string $zone
     * @param string $name
     * @param string|null $id
     * @return string
     */
    public static function getHashKeyName (string $zone, string $name, ?string $id = null) : string {
//        return 'zone:' . $zone . ';' . $name . ($id ? ':' . $id : '');
        return $name . ($id ? ':' . $id : '');
    }

    /**
     * Convert the entity to an array.
     *
     * @return array
     * @deprecated use toArray instead
     */
    public function getArray () {
        return $this->toArray();
    }

    /**
     * Convert the entity to an array.
     *
     * @return array
     */
    public function toArray () {
        $values = [];

        foreach ($this->getFields() as $field) {
            $values[$field] = $this->getValue($field);
        }

        return $values;
    }

    /**
     * Convenience function for debugging entities.
     *
     * @return string
     */
    public function __toString () {
        $str = '';

        foreach ($this->getFields() as $field) {
            $str .= ($this->isFieldDirty($field) ? '*' : '') . $field . ' = ' . $this->getValue($field) . PHP_EOL;
        }

        return $str;
    }

    /**
     * @return ClientInterface
     */
    public function getClient () {
        return $this->client;
    }

    /**
     * @param ClientInterface $client
     */
    public function setClient (ClientInterface $client) {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getName () : string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    protected function setName (string $name) : void {
        if (strlen($name) == 0) {
            throw new InvalidArgumentException('Name must be non-empty string');
        }

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId () : string {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId (string $id) : void {
        if (strlen($id) == 0) {
            throw new InvalidArgumentException('ID must be non-empty string');
        }

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getZone () : string {
        return $this->zone;
    }

    /**
     * @param string $zone
     */
    public function setZone (string $zone) : void {
        if (strlen($zone) == 0) {
            throw new InvalidArgumentException('Zone must be non-empty string');
        }

        $this->zone = $zone;
    }
}