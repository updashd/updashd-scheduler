<?php
namespace Updashd\Scheduler\Model;

use InvalidArgumentException;
use Updashd\Scheduler\Communication\ClientInterface;
use Updashd\Scheduler\ScheduleType;
use Updashd\Scheduler\State;

class Task extends AbstractHashEntity {
    const NAME = 'task';
    const FIELD_ENABLED = 'Enabled';
    const FIELD_STATE = 'State';
    const FIELD_SCHEDULED_TIME = 'ScheduledTime';
    const FIELD_LAST_COMPLETION_TIME = 'LastCompletionTime';
    const FIELD_TIMEOUT = 'Timeout';
    const FIELD_SCHEDULE_TYPE = 'ScheduleType';
    const FIELD_CRON = 'Cron';
    const FIELD_INTERVAL = 'Interval';
    const FIELD_RUN_COUNT = 'RunCount';
    const FIELD_CONFIG = 'Config';
    const FIELD_SERVICE  = 'Service';
    const FIELD_ACCOUNT_ID  = 'AccountId';
    
    private $fields = [
        self::FIELD_ENABLED,
        self::FIELD_STATE,
        self::FIELD_SCHEDULED_TIME,
        self::FIELD_LAST_COMPLETION_TIME,
        self::FIELD_TIMEOUT,
        self::FIELD_SCHEDULE_TYPE,
        self::FIELD_CRON,
        self::FIELD_INTERVAL,
        self::FIELD_RUN_COUNT,
        self::FIELD_CONFIG,
        self::FIELD_SERVICE,
        self::FIELD_ACCOUNT_ID
    ];
    
    private $defaults = [
        self::FIELD_ENABLED => true,
        self::FIELD_STATE => State::STATE_UNDEFINED,
        self::FIELD_SCHEDULED_TIME => null,
        self::FIELD_LAST_COMPLETION_TIME => null,
        self::FIELD_TIMEOUT => null,
        self::FIELD_SCHEDULE_TYPE => ScheduleType::TYPE_ONCE,
        self::FIELD_CRON => null,
        self::FIELD_INTERVAL => null,
        self::FIELD_RUN_COUNT => 0,
        self::FIELD_CONFIG => null,
        self::FIELD_SERVICE => 'default',
        self::FIELD_ACCOUNT_ID => null
    ];
    
    public function __construct (ClientInterface $client, $zone, $id) {
        parent::__construct($client, $zone, self::NAME, $id, $this->fields, $this->defaults);
    }
    
    public static function getTaskKeyName($zone, $id) {
        return parent::getHashKeyName($zone, self::NAME, $id);
    }
    
    /**
     * @return bool
     */
    public function getEnabled () : bool {
        return $this->getValue(self::FIELD_ENABLED);
    }
    
    /**
     * @param bool $enabled
     */
    public function setEnabled (bool $enabled) : void {
        $this->setValue(self::FIELD_ENABLED, $enabled);
    }
    
    /**
     * @return string one of the State::STATE_* constants
     */
    public function getState () : string {
        return $this->getValue(self::FIELD_STATE);
    }
    
    /**
     * @param string $state State::STATE_* constant
     */
    public function setState (string $state) : void {
        if (! State::isState($state)) {
            throw new InvalidArgumentException('State is invalid');
        }

        $this->setValue(self::FIELD_STATE, $state);
    }
    
    /**
     * @return int Unix Timestamp
     */
    public function getScheduledTime () : ?int {
        $value = $this->getValue(self::FIELD_SCHEDULED_TIME);

        return $value ? (int) $value : null;
    }
    
    /**
     * @param int|null $scheduledTime Unix Timestamp
     */
    public function setScheduledTime (?int $scheduledTime) : void {
        $this->setValue(self::FIELD_SCHEDULED_TIME, $scheduledTime);
    }

    /**
     * @return int Unix Timestamp
     */
    public function getLastCompletionTime () : ?int {
        $value = $this->getValue(self::FIELD_LAST_COMPLETION_TIME);

        return $value ? (int) $value : null;
    }

    /**
     * @param int|null $lastCompletionTime
     */
    public function setLastCompletionTime (?int $lastCompletionTime) : void {
        $this->setValue(self::FIELD_LAST_COMPLETION_TIME, $lastCompletionTime);
    }
    
    /**
     * @return int|null
     */
    public function getTimeout () : ?int {
        $value = $this->getValue(self::FIELD_TIMEOUT);

        return $value ? (int) $value : null;
    }
    
    /**
     * @param int|null $timeout
     */
    public function setTimeout (?int $timeout) : void {
        $this->setValue(self::FIELD_TIMEOUT, $timeout);
    }
    
    /**
     * @return string ScheduleType::TYPE_* constant
     */
    public function getScheduleType () : string {
        return $this->getValue(self::FIELD_SCHEDULE_TYPE);
    }
    
    /**
     * @param string $scheduleType on of the ScheduleType::TYPE_* constants
     */
    public function setScheduleType (string $scheduleType) : void {
        if (! ScheduleType::isScheduleType($scheduleType)) {
            throw new InvalidArgumentException('Schedule Type invalid: ' . $scheduleType);
        }

        $this->setValue(self::FIELD_SCHEDULE_TYPE, $scheduleType);
    }
    
    /**
     * @return string|null
     */
    public function getCron () : ?string {
        return $this->getValue(self::FIELD_CRON);
    }
    
    /**
     * @param string|null $cron a valid Cron syntax schedule
     */
    public function setCron (string $cron) : void {
        $this->setValue(self::FIELD_CRON, $cron);
    }
    
    /**
     * @return string|null the interval in seconds
     */
    public function getInterval () : ?string {
        return $this->getValue(self::FIELD_INTERVAL);
    }
    
    /**
     * @param int $interval the interval in seconds
     */
    public function setInterval (?int $interval) : void {
        $this->setValue(self::FIELD_INTERVAL, $interval);
    }
    
    /**
     * @return int how many times the task has run
     */
    public function getRunCount () : int {
        return (int) $this->getValue(self::FIELD_RUN_COUNT);
    }
    
    /**
     * @param int $runCount
     */
    public function setRunCount (int $runCount) {
        $this->setValue(self::FIELD_RUN_COUNT, $runCount);
    }
    
    /**
     * @return string|null configuration for the task
     */
    public function getConfig () : ?string {
        return $this->getValue(self::FIELD_CONFIG);
    }
    
    /**
     * @param string|null $config configuration for the task
     */
    public function setConfig (?string $config) : void {
        $this->setValue(self::FIELD_CONFIG, $config);
    }
    
    /**
     * @return string
     */
    public function getService () : ?string {
        return $this->getValue(self::FIELD_SERVICE);
    }
    
    /**
     * @param string $service
     */
    public function setService (?string $service) {
        $this->setValue(self::FIELD_SERVICE, $service);
    }
    
    /**
     * @return string
     */
    public function getAccountId () : ?string {
        return $this->getValue(self::FIELD_ACCOUNT_ID);
    }
    
    /**
     * @param string $accountId
     */
    public function setAccountId (?string $accountId) {
        $this->setValue(self::FIELD_ACCOUNT_ID, $accountId);
    }
}