<?php
namespace Updashd\Scheduler\Model;

use Updashd\Scheduler\Communication\ClientInterface;

class Result extends AbstractHashEntity {
    const NAME = 'result';
    const FIELD_TASK_ID = 'TaskId';
    const FIELD_START_TIME = 'StartTime';
    const FIELD_END_TIME = 'EndTime';
    const FIELD_ELAPSED_TIME = 'ElapsedTime';
    const FIELD_RESULT = 'Result';
    
    const ID_SEPARATOR = '-';
    
    private $fields = [
        self::FIELD_TASK_ID,
        self::FIELD_START_TIME,
        self::FIELD_END_TIME,
        self::FIELD_ELAPSED_TIME,
        self::FIELD_RESULT
    ];
    
    private $defaults = [
        self::FIELD_TASK_ID => null,
        self::FIELD_START_TIME => null,
        self::FIELD_END_TIME => null,
        self::FIELD_ELAPSED_TIME => null,
        self::FIELD_RESULT => null
    ];
    
    public static function getResultIdByTaskIdAndTimestamp($taskId, $timestamp) {
        return $taskId . self::ID_SEPARATOR . $timestamp;
    }
    
    public static function getByTaskIdAndTimestampFromResultId($resultId) {
        return explode(self::ID_SEPARATOR, $resultId);
    }
    
    public function __construct (ClientInterface $client, $zone, $taskIdOrResultId, $timestamp = null) {
        if ($timestamp) {
            $id = self::getResultIdByTaskIdAndTimestamp($taskIdOrResultId, $timestamp);
        }
        else {
            $id = $taskIdOrResultId;
        }
        
        parent::__construct($client, $zone, self::NAME, $id, $this->fields, $this->defaults);
    }
    
    public static function getResultKeyNameFromResultId ($zone, $resultId) {
        return parent::getHashKeyName($zone, self::NAME, $resultId);
    }
    
    public static function getResultKeyNameFromTimestamp($zone, $taskId, $timestamp) {
        $id = self::getResultIdByTaskIdAndTimestamp($taskId, $timestamp);
        return self::getResultKeyNameFromResultId($zone, $id);
    }
    
    /**
     * @return int
     */
    public function getStartTime () {
        return $this->getValue(self::FIELD_START_TIME);
    }
    
    /**
     * @param int $startTime
     */
    public function setStartTime ($startTime) {
        $this->setValue(self::FIELD_START_TIME, $startTime);
    }
    
    /**
     * @return int
     */
    public function getEndTime () {
        return $this->getValue(self::FIELD_END_TIME);
    }
    
    /**
     * @param int $endTime
     */
    public function setEndTime ($endTime) {
        $this->setValue(self::FIELD_END_TIME, $endTime);
    }
    
    /**
     * @return int
     */
    public function getElapsedTime () {
        return $this->getValue(self::FIELD_ELAPSED_TIME);
    }
    
    /**
     * @param int $elapsedTime
     */
    public function setElapsedTime ($elapsedTime) {
        $this->setValue(self::FIELD_ELAPSED_TIME, $elapsedTime);
    }
    
    /**
     * @return mixed
     */
    public function getResult () {
        return $this->getValue(self::FIELD_RESULT);
    }
    
    /**
     * @param mixed $result
     */
    public function setResult ($result) {
        $this->setValue(self::FIELD_RESULT, $result);
    }
    
    /**
     * @return mixed
     */
    public function getTaskId () {
        return $this->getValue(self::FIELD_TASK_ID);
    }
    
    public function setTaskId ($taskId) {
        $this->setValue(self::FIELD_TASK_ID, $taskId);
    }
}