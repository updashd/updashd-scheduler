<?php
namespace Updashd\Scheduler;

/**
 * The states a task can be in.
 *
 * @package Updashd\Scheduler
 */
class State {
    const STATE_RESCHEDULING = 'Rescheduling';
    const STATE_SCHEDULED = 'Scheduled';
    const STATE_PENDING = 'Pending';
    const STATE_ERROR = 'Error';
    const STATE_PROCESSING = 'Processing';
    const STATE_COMPLETE = 'Complete';
    const STATE_DISABLED = 'Disabled';
    const STATE_UNDEFINED = 'Undefined';

    public static function getStates () : array {
        return [
            self::STATE_RESCHEDULING,
            self::STATE_SCHEDULED,
            self::STATE_PENDING,
            self::STATE_ERROR,
            self::STATE_PROCESSING,
            self::STATE_COMPLETE,
            self::STATE_DISABLED,
            self::STATE_UNDEFINED
        ];
    }

    public static function isState (string $input) : bool {
        return in_array($input, self::getStates());
    }
}