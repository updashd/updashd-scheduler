<?php

namespace Updashd\Scheduler;

use Predis\ClientInterface;
use Predis\PubSub\AbstractConsumer;
use Updashd\Scheduler\Communication\PredisClientProxy;
use Updashd\Scheduler\Model\Error;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Model\Result;
use Updashd\Scheduler\Popo\Service;
use Updashd\Scheduler\Scheduler\SchedulerImpl;
use Updashd\Scheduler\Scheduler\SchedulerInterface;

class Scheduler implements SchedulerInterface {

    /** @var \Updashd\Scheduler\Scheduler\SchedulerInterface */
    private $scheduler;

    /**
     * Scheduler constructor.
     *
     * @param \Predis\ClientInterface $client
     * @param string $zone
     */
    public function __construct (ClientInterface $client, string $zone) {
        $this->scheduler = new SchedulerImpl(new PredisClientProxy($client), $zone);
    }


    /**
     * Create a task model for the zone and return it.
     *
     * @param string $taskId
     * @return Task
     */
    public function createTask (string $taskId) : Task {
        return $this->scheduler->createTask($taskId);
    }

    /**
     * Create a result model.
     *
     * @param string $taskIdOrResultId
     * @param int|null $timestamp Unix timestamp
     * @return Result
     */
    public function createResult (string $taskIdOrResultId, int $timestamp = null) : Result {
        return $this->scheduler->createResult($taskIdOrResultId, $timestamp);
    }

    /**
     * Create an error model
     *
     * @param string $taskId
     * @return Error
     */
    public function createError (string $taskId) : Error {
        return $this->scheduler->createError($taskId);
    }

    /**
     * Queue up a service to be registered.
     *
     * @param Service $serviceMetricFields
     */
    public function registerService (Service $serviceMetricFields) : void {
        $this->scheduler->registerService($serviceMetricFields);
    }

    /**
     * Add a task to the scheduler.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function addTask (Task $task) : void {
        $this->scheduler->addTask($task);
    }

    /**
     * Get a task from the queue. BLOCKING!
     *
     * @param string|string[] $services Service module names to process the queues for.
     * @return Task
     */
    public function getTask ($services) : Task {
        return $this->scheduler->getTask($services);
    }

    /**
     * @param string|string[] $services
     * @return \Updashd\Scheduler\Model\Task|null
     */
    public function getTaskNonBlocking (array $services) : ?Task {
        return $this->scheduler->getTaskNonBlocking($services);
    }

    /**
     * Get a service registration from the queue. BLOCKING!
     *
     * @return Service
     */
    public function getServiceRegistrationBlocking () : Service {
        return $this->scheduler->getServiceRegistrationBlocking();
    }

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @deprecated
     * @param int $timeout in seconds
     * @return bool|Service Service if one is ready, false otherwise.
     */
    public function getServiceRegistrationQuick (int $timeout = 2) {
        return $this->scheduler->getServiceRegistrationQuick($timeout);
    }

    /**
     * Get a service registration from the queue. Non-blocking!
     *
     * @param int $timeout in seconds
     * @return null|Service Service if one is ready, null otherwise.
     */
    public function getServiceRegistrationOrTimeout (int $timeout = 2) : ?Service {
        return $this->scheduler->getServiceRegistrationOrTimeout($timeout);
    }

    /**
     * Mark a task as being processed.
     *
     * @param Task $task
     * @return Result
     * @throws \Exception
     */
    public function markProcessing (Task $task) : Result {
        return $this->scheduler->markProcessing($task);
    }

    /**
     * Mark a task as being complete.
     *
     * @param Task $task
     * @throws \Exception
     */
    public function markComplete (Task $task) : void {
        $this->scheduler->markComplete($task);
    }

    /**
     * Record the result of a task.
     *
     * @param Result $result
     * @throws \Exception
     */
    public function recordResult (Result $result) : void {
        $this->scheduler->recordResult($result);
    }

    /**
     * Create and record an error.
     *
     * @param string $id
     * @param string $code Error code
     * @param string $details Details of the error
     */
    public function createAndRecordError (string $id, string $code, string $details) : void {
        $this->scheduler->createAndRecordError($id, $code, $details);
    }

    /**
     * Record an error.
     *
     * @param Error $error
     */
    public function recordError (Error $error) : void {
        $this->scheduler->recordError($error);
    }

    /**
     * Push an incident ID to the incident queue.
     *
     * @param string $incidentId
     */
    public function pushIncident (string $incidentId) : void {
        $this->scheduler->pushIncident($incidentId);
    }

    /**
     * Remove a task from the scheduler. This includes removing it from whatever queue it is in according to it's status.
     *
     * @param string $taskId
     * @return bool true if successful, false otherwise
     */
    public function removeTask (string $taskId) : bool {
        return $this->scheduler->removeTask($taskId);
    }

    /**
     * Determine if there is a task for a given id
     *
     * @param string $taskId
     * @return bool
     */
    public function hasTask (string $taskId) : bool {
        return $this->scheduler->hasTask($taskId);
    }

    /**
     * Publish a result to be consumed by event subscribers.
     *
     * @param Result $result
     */
    public function publishResult (Result $result) : void {
        $this->scheduler->publishResult($result);
    }

    /**
     * Get an incident from the queue. BLOCKING!
     *
     * @return int|null
     */
    public function getIncident () : ?int {
        return $this->scheduler->getIncident();
    }

    /**
     * Subscribe to results from the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeResult (array $taskIds) : ?AbstractConsumer {
        return $this->scheduler->subscribeResult($taskIds);
    }

    /**
     * Subscribe to changes in state for the given task IDs.
     *
     * @param string[] $taskIds
     * @return null|\Predis\PubSub\Consumer
     */
    public function subscribeStateChange ($taskIds) : ?AbstractConsumer {
        return $this->scheduler->subscribeStateChange($taskIds);
    }

    /**
     * Process the schedule. Move tasks that are scheduled to be run into the pending queue.
     *
     * @param int $timestamp the current unix timestamp
     * @return int the count of how many tasks were queued up
     */
    public function processSchedule (int $timestamp) : int {
        return $this->scheduler->processSchedule($timestamp);
    }

    /**
     * Process rescheduling tasks
     *
     * @param bool $forever
     * @param int $timeout
     */
    public function processReschedule (bool $forever = true, int $timeout = 30) : void {
        $this->scheduler->processReschedule($forever, $timeout);
    }

    /**
     * Move items from the processing queue back into the pending queue if they have not been completed.
     *
     * @param int $timestamp the current timestamp
     * @param int $zombieTimeout the number of seconds a task can sit before being considered a zombie.
     * @return int
     */
    public function processZombies (int $timestamp, int $zombieTimeout = 30) : int {
        return $this->scheduler->processZombies($timestamp, $zombieTimeout);
    }

    /**
     * @return null|Error|Result
     */
    public function getResultOrError () {
        return $this->scheduler->getResultOrError();
    }

    /**
     * @param int $timeout
     * @return null|Error|Result
     */
    public function getResultOrErrorQuick (int $timeout = 2) {
        return $this->scheduler->getResultOrErrorQuick($timeout);
    }

    /**
     * Mark a result as recorded. This should be called from the recorder only.
     *
     * @param Result $result
     */
    public function markResultRecorded (Result $result) : void {
        $this->scheduler->markResultRecorded($result);
    }

    /**
     * Mark a error as recorded. This should be called from the recorder only.
     *
     * @param Error $error
     */
    public function markErrorReported (Error $error) : void {
        $this->scheduler->markErrorReported($error);
    }

    public function getRescheduleSize () : int {
        return $this->scheduler->getRescheduleSize();
    }

    public function getScheduleSize () : int {
        return $this->scheduler->getScheduleSize();
    }

    public function getPendingSize ($service) : int {
        return $this->scheduler->getPendingSize($service);
    }

    public function getProcessingSize () : int {
        return $this->scheduler->getProcessingSize();
    }

    public function getCompleteSize () : int {
        return $this->scheduler->getCompleteSize();
    }

    public function getErrorSize () : int {
        return $this->scheduler->getErrorSize();
    }

    public function getIncidentSize () : int {
        return $this->scheduler->getIncidentSize();
    }

    public function getRegisterFieldSize () : int {
        return $this->scheduler->getRegisterFieldSize();
    }

    public function startTransaction () : void {
        $this->scheduler->startTransaction();
    }

    public function commitTransaction () : void {
        $this->scheduler->commitTransaction();
    }

    public function discardTransaction () : void {
        $this->scheduler->discardTransaction();
    }

    public function getTaskEntity (string $taskId, bool $retrieveNow = true) : Task {
        return $this->scheduler->getTaskEntity($taskId, $retrieveNow);
    }

    public function getResultEntity (string $resultId, bool $retrieveNow = true) : Result {
        return $this->scheduler->getResultEntity($resultId, $retrieveNow);
    }

    public function getErrorEntity (string $taskId, bool $retrieveNow = true) : Error {
        return $this->scheduler->getErrorEntity($taskId, $retrieveNow);
    }
}