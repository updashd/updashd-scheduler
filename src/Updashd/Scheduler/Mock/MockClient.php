<?php

namespace Updashd\Scheduler\Mock;

use RuntimeException;
use Updashd\Scheduler\Communication\ClientInterface;

class MockClient implements ClientInterface {

    private $keys = [];
    private $isMulti = false;

    private function existsNotExpired($key) : bool {
        if (! array_key_exists($key, $this->keys)) {
            return false;
        }

        $exp = $this->keys[$key]['expiration'];

        if ($exp != 0 && $exp <= time()) {
            unset($this->keys[$key]);
            return false;
        }

        return true;
    }

    private function touchKey($key, $default = null) {
        if (! array_key_exists($key, $this->keys)) {
            $this->keys[$key]['data'] = $default;
            $this->keys[$key]['expiration'] = 0;
        }
    }

    private function checkValidKey($key) {
        if (strlen($key) == 0) {
            throw new RuntimeException('Key invalid');
        }
    }

    public function expire($key, $seconds) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            $this->keys[$key]['expiration'] = time() + $seconds;
            return 1;
        }
        return 0;
    }

    public function persist($key) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            $this->keys[$key]['expiration'] = 0;
            return 1;
        }
        return 0;
    }

    public function ttl($key) : int {
        $this->checkValidKey($key);
        if (! $this->existsNotExpired($key)) {
            return -2;
        }

        if ($this->keys[$key]['expiration'] == 0) {
            return -1;
        }

        return $this->keys[$key]['expiration'] - time();
    }

    public function brpop($keys, $timeout) : ?array {
        $keys = (! is_array($keys) ? [$keys] : $keys);

        foreach ($keys as $key) {
            $this->checkValidKey($key);
            if ($this->existsNotExpired($key) && $this->keys[$key]['data']) {
                return [$key, array_pop($this->keys[$key]['data'])];
            }
        }

        if ($timeout == 0) {
            throw new \RuntimeException('Indefinate timeout not allowed');
        }

        sleep($timeout);

        return null;
    }

    /**
     * @param $key
     * @param array|string $values
     * @return int
     */
    public function lpush($key, $values) : int {
        $this->checkValidKey($key);
        $this->touchKey($key, []);
        $values = is_array($values) ? $values : [$values];
        $cnt = 0;
        foreach ($values as $value) {
            array_unshift($this->keys[$key]['data'], (string) $value);
            $cnt++;
        }
        return $cnt;
    }

    public function lrem($key, $count, $value) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            $cnt = count($this->keys[$key]['data']);
            $indexesToRemove = [];
            // Remove from beginning (left-hand)
            if ($count > 0) {
                for ($i = 0; $i < $cnt && $count > 0; $i++) {
                    $elem = $this->keys[$key]['data'][$i];
                    if ($elem == $value) {
                        $indexesToRemove[] = $i;
                        $count--;
                    }
                }
            }
            // Remove from end (right-hand)
            elseif ($count < 0) {
                for ($i = $cnt - 1; $i >= 0 && $count < 0; $i--) {
                    $elem = $this->keys[$key]['data'][$i];
                    if ($elem == $value) {
                        $indexesToRemove[] = $i;
                        $count++;
                    }
                }
            }
            // Remove all
            else {
                for ($i = 0; $i < $cnt; $i++) {
                    $elem = $this->keys[$key]['data'][$i];
                    if ($elem == $value) {
                        $indexesToRemove[] = $i;
                    }
                }
            }

            if ($indexesToRemove) {
                rsort($indexesToRemove);

                // Remove values
                foreach ($indexesToRemove as $index) {
                    unset($this->keys[$key]['data'][$index]);
                }

                // Re-index
                $this->keys[$key]['data'] = array_values($this->keys[$key]['data']);
            }

            return count($indexesToRemove);
        }

        return 0;
    }

    public function llen ($key) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            return count($this->keys[$key]['data']);
        }
        return 0;
    }

    public function ldebug($key) : array {
        if ($this->existsNotExpired($key)) {
            return $this->keys[$key]['data'];
        }
        return [];
    }

    public function hlen ($key) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            return count($this->keys[$key]['data']);
        }

        return 0;
    }

    public function hget($key, $field) : ?string {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key) && array_key_exists($field, $this->keys[$key]['data'])) {
            return $this->keys[$key]['data'][$field];
        }

        return null;
    }

    public function hset($key, $field, $value) : int {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key) && array_key_exists($field, $this->keys[$key]['data'])) {
            $this->keys[$key]['data'][$field] = (string) $value;
            return 0;
        }
        else {
            $this->touchKey($key, []);
            $this->keys[$key]['data'][$field] = (string) $value;
            return 1;
        }
    }

    public function hincrby($key, $field, $increment) : int {
        $this->checkValidKey($key);
        if (! $this->existsNotExpired($key) || ! array_key_exists($field, $this->keys[$key]['data'])) {
            $this->touchKey($key, []);
            $this->keys[$key]['data'][$field] = 0;
        }

        $this->keys[$key]['data'][$field] += $increment;
        return $this->keys[$key]['data'][$field];
    }

    public function hmget($key, array $fields) : array {
        $this->checkValidKey($key);
        $result = [];
        foreach ($fields as $field) {
            $result[] = $this->hget($key, $field);
        }
        return $result;
    }

    public function hmset($key, array $dictionary) {
        $this->checkValidKey($key);
        $this->touchKey($key, []);

        foreach ($dictionary as $field => $value) {
            $result[] = $this->hset($key, $field, $value);
        }

        return 'OK';
    }

    public function zadd($key, array $membersAndScoresDictionary) : int {
        $this->checkValidKey($key);
        $this->touchKey($key, []);
        $cnt = 0;
        foreach ($membersAndScoresDictionary as $member => $score) {
            $member = (string) $member;

            if (! is_numeric($score)) {
                throw new RuntimeException('score must both be numeric');
            }
            if (floor($score) != $score) {
                throw new RuntimeException('non-integer scores not supported by mock');
            }

            // Remove it if it exists
            if ($this->zrem($key, $member) > 0) {
                $cnt--;
            }

            if (! array_key_exists($score, $this->keys[$key]['data'])) {
                $this->keys[$key]['data'][$score] = [];
            }

            if (! in_array($member, $this->keys[$key]['data'][$score])) {
                $this->keys[$key]['data'][$score][] = $member;
                $cnt++;
                sort($this->keys[$key]['data'][$score]);
            }
        }
        asort($this->keys[$key]['data']);
        return $cnt;
    }

    public function zrem($key, $member) : int {
        $this->checkValidKey($key);
        $cnt = 0;
        foreach ($this->keys[$key]['data'] as $score => $members) {
            foreach ($members as $eKey => $eMember) {
                if ($member == $eMember) {
                    unset($this->keys[$key]['data'][$score][$eKey]);
                    sort($this->keys[$key]['data'][$score]);
                    $cnt++;

                    // Remove score if there are no members anymore
                    if (count($this->keys[$key]['data'][$score]) == 0) {
                        unset($this->keys[$key]['data'][$score]);
                    }
                }
            }
        }
        return $cnt;
    }

    public function zrevrangebyscore($key, $max, $min, array $options = []) : array {
        if ($options) {
            throw new RuntimeException('options not implemented');
        }

        if (! is_numeric($max) || ! is_numeric($min)) {
            throw new RuntimeException('max and min must both be numeric');
        }
        if (floor($max) != $max || floor($min) != $min) {
            throw new RuntimeException('non-integer scores not supported by mock');
        }

        $this->checkValidKey($key);
        $ret = [];

        krsort($this->keys[$key]['data']);

        foreach ($this->keys[$key]['data'] as $score => $members) {
            if ($score <= $max && $score >= $min) {
                $ret = array_merge($ret, $members);
            }
        }

        ksort($this->keys[$key]['data']);

        return $ret;
    }

    public function zcard($key) : int {
        $this->checkValidKey($key);
        $cnt = 0;
        if ($this->existsNotExpired($key)) {
            foreach ($this->keys[$key]['data'] as $score => $members) {
                foreach ($members as $eKey => $eMember) {
                    $cnt++;
                }
            }
        }
        return $cnt;
    }

    public function zdebug($key) {
        $this->checkValidKey($key);
        if ($this->existsNotExpired($key)) {
            return $this->keys[$key]['data'];
        }
        return null;
    }

    public function publish($channel, $message) : int {
        return -1;
    }

    public function multi() : void {
        $this->isMulti = true;
    }

    public function exec() : void {
        if (! $this->isMulti) {
            throw new RuntimeException('MULTI was never called, cannot EXEC');
        }

        $this->isMulti = false;
    }

    public function discard() : void {
        if (! $this->isMulti) {
            throw new RuntimeException('MULTI was never called, cannot DISCARD');
        }

        $this->isMulti = false;
    }

    public function debugIsMulti () {
        return $this->isMulti;
    }
}