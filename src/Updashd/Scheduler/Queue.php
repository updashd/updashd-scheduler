<?php
namespace Updashd\Scheduler;

class Queue {
    const PREFIX_ZONE = 'zone:';
    const PREFIX_SERVICE = 'service:';
    const PREFIX_QUEUE = 'queue:';
    
    const SEPARATOR = ';';
    
    const QUEUE_RESCHEDULE = 'Reschedule';
    const QUEUE_PENDING = 'Pending';
    const QUEUE_COMPLETE = 'Complete';
    const QUEUE_ERROR = 'Error';
    const QUEUE_INCIDENT = 'Incident';

    const QUEUE_SERVICE_REGISTER = 'ServiceRegister';

    const SET_SCHEDULE = 'Schedule';
    const SET_PROCESSING = 'Processing';
    
    public static function getZoneKeyName (string $zone, string $queue) : string  {
        return self::PREFIX_ZONE . $zone . ($queue ? self::SEPARATOR . self::PREFIX_QUEUE . $queue : '');
    }
    
    public static function getZoneServiceKeyName (string $zone, string $service, string $queue = null) : string  {
        return self::PREFIX_ZONE . $zone . self::SEPARATOR . self::PREFIX_SERVICE  . $service . ($queue ? self::SEPARATOR . self::PREFIX_QUEUE . $queue : '');
    }
    
    public static function getRescheduleKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::QUEUE_RESCHEDULE);
    }
    
    public static function getScheduleKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::SET_SCHEDULE);
    }
    
    public static function getPendingKeyName (string $zone, string $service) : string  {
        return self::getZoneServiceKeyName($zone, $service, Queue::QUEUE_PENDING);
    }
    
    public static function getProcessingKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::SET_PROCESSING);
    }
    
    public static function getCompleteKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::QUEUE_COMPLETE);
    }
    
    public static function getErrorKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::QUEUE_ERROR);
    }

    public static function getIncidentKeyName (string $zone) : string  {
        return self::getZoneKeyName($zone, Queue::QUEUE_INCIDENT);
    }

    public static function getServiceRegisterKeyName (string $zone) : string {
        return self::getZoneKeyName($zone, Queue::QUEUE_SERVICE_REGISTER);
    }
}