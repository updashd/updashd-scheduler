<?php

namespace Updashd\Scheduler\Communication;

interface ClientInterface {

    public function expire ($key, $seconds) : int;

    public function persist ($key) : int;

    public function ttl ($key) : int;

    /**
     * @param string|string[] $keys
     * @param int $timeout
     * @return array|null
     */
    public function brpop ($keys, $timeout);

    /**
     * @param string $key
     * @param string|string[] $values
     * @return int
     */
    public function lpush ($key, $values) : int;

    public function lrem ($key, $count, $value) : int;

    public function llen ($key) : int;

    public function hlen ($key) : int;

    public function hget ($key, $field) : ?string;

    public function hset ($key, $field, $value) : int;

    public function hincrby ($key, $field, $increment) : int;

    public function hmget ($key, array $fields) : array;

    public function hmset ($key, array $dictionary);

    public function zadd ($key, array $membersAndScoresDictionary) : int;

    public function zrem ($key, $member) : int;

    public function zrevrangebyscore ($key, $max, $min, array $options = []) : array;

    public function zcard ($key) : int;

    public function publish ($channel, $message) : int;

    public function multi () : void;

    public function exec () : void;

    public function discard () : void;
}